

CREATE TABLE `bill_item_master` (
  `bim_id` int(10) NOT NULL AUTO_INCREMENT,
  `billid` int(10) NOT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` float(10,2) NOT NULL,
  `order_time` varchar(255) NOT NULL,
  `user_by` varchar(255) NOT NULL,
  `parcel` enum('0','1') NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`bim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;


INSERT INTO bill_item_master VALUES
("1","1601","6","1","40","40.00","04:39:47","admin","0","0"),
("2","1601","6","1","40","40.00","04:39:52","admin","0","0"),
("4","1601","3","1","70","70.00","04:58:38","admin","0","0"),
("5","1601","10","1","140","140.00","04:58:40","admin","0","0"),
("6","1601","6","1","40","40.00","04:58:44","admin","0","0"),
("7","1601","9","1","90","90.00","04:58:52","admin","0","0"),
("8","1601","1","1","40","40.00","04:58:54","admin","0","0"),
("9","1602","10","3","140","420.00","16:59:12","admin","0","0"),
("10","1603","8","1","60","60.00","05:00:31","admin","0","0"),
("11","1604","8","3","60","180.00","05:04:39","admin","1","0"),
("18","1605","28","1","50","50.00","06:12:25","admin","0","0"),
("19","1605","28","1","500","125.00","06:12:39","admin","0","250"),
("20","1605","28","1","500","250.00","06:13:57","admin","0","500"),
("21","1605","28","1","500","500.00","06:14:44","admin","0","500"),
("22","1605","21","1","5555","5555.00","06:15:25","admin","0","0"),
("32","1605","28","1","500","500.00","06:27:45","admin","0","1000"),
("33","1605","28","1","500","375.00","06:27:52","admin","0","750"),
("35","1601","8","9","60","540.00","07:22:25","admin","0","0"),
("36","1601","11","1","140","140.00","07:22:38","admin","0","0"),
("37","1603","22","1","555","555.00","07:22:47","admin","0","0"),
("38","1603","6","1","40","40.00","07:22:59","admin","0","0"),
("39","1606","3","1","70","70.00","07:23:08","admin","0","0"),
("40","1604","28","1","500","250.00","07:23:39","admin","0","500"),
("43","1607","4","2","35","70.00","","","0","0"),
("45","1607","8","1","60","60.00","03:49:04","admin","0","0"),
("46","1607","5","1","30","30.00","03:49:05","admin","0","0"),
("47","1607","7","5","130","650.00","03:49:07","admin","0","0"),
("48","1607","28","5","50","250.00","03:49:10","admin","0","0"),
("54","1608","10","2","140","280.00","","","0","0"),
("55","1608","5","2","30","60.00","","","0","0"),
("56","1609","12","2","140","280.00","","","0","0"),
("57","1610","11","1","140","140.00","","","0","0"),
("58","1610","9","2","90","180.00","","","0","0"),
("59","1610","2","3","50","150.00","","","0","0"),
("60","1611","6","5","40","200.00","","","0","0"),
("61","1612","4","3","35","105.00","","","0","0"),
("62","1613","11","2","140","280.00","","","0","0"),
("63","1613","5","2","30","60.00","","","0","0"),
("64","1614","11","2","140","280.00","","","0","0"),
("65","1615","5","3","30","90.00","","","0","0"),
("66","1616","7","2","130","260.00","","","0","0"),
("67","1617","4","3","35","105.00","","","0","0"),
("68","1617","5","3","30","90.00","","","0","0"),
("69","1618","5","3","30","90.00","","","0","0"),
("70","1618","10","3","140","420.00","","","0","0"),
("71","1619","7","2","130","260.00","","","0","0"),
("72","1620","5","3","30","90.00","","","0","0"),
("73","1621","5","3","30","90.00","","","0","0"),
("74","1621","10","3","140","420.00","","","0","0"),
("75","1622","4","2","35","70.00","","","0","0"),
("76","1623","4","2","35","70.00","","","0","0"),
("77","1624","4","2","35","70.00","","","0","0"),
("78","1625","11","3","140","420.00","","","0","0"),
("79","1625","9","2","90","180.00","","","0","0"),
("80","1625","2","1","50","50.00","","","0","0"),
("81","1626","6","5","40","200.00","","","0","0"),
("82","1627","6","3","40","120.00","","","0","0"),
("83","1628","5","3","30","90.00","","","0","0"),
("84","1629","9","2","90","180.00","","","0","0"),
("85","1629","8","10","60","600.00","","","0","0"),
("86","1629","8","50","60","3000.00","","","0","0"),
("87","1630","9","2","90","180.00","","","0","0"),
("88","1630","8","10","60","600.00","","","0","0"),
("89","1630","8","50","60","3000.00","","","0","0"),
("90","1631","10","2","140","280.00","","","0","0"),
("91","1631","5","2","30","60.00","","","0","0"),
("92","1632","4","2","35","70.00","","","0","0");




CREATE TABLE `bill_item_master_original` (
  `bim_id` int(10) NOT NULL AUTO_INCREMENT,
  `billid` int(10) NOT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` float(10,2) NOT NULL,
  PRIMARY KEY (`bim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


INSERT INTO bill_item_master_original VALUES
("1","1","11","5","140","700.00"),
("2","1","11","5","140","700.00");




CREATE TABLE `bill_master` (
  `billid` int(11) NOT NULL AUTO_INCREMENT,
  `bill_name` varchar(100) NOT NULL,
  `bill_mobile` varchar(50) NOT NULL,
  `bill_table` varchar(100) DEFAULT NULL,
  `bill_waiter` varchar(100) DEFAULT NULL,
  `table_part` varchar(5) NOT NULL,
  `bill_status` varchar(25) NOT NULL,
  `bill_notes` varchar(255) NOT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL,
  `t_status` enum('L','C','R') NOT NULL,
  `update_time` time NOT NULL,
  PRIMARY KEY (`billid`)
) ENGINE=InnoDB AUTO_INCREMENT=1633 DEFAULT CHARSET=latin1;


INSERT INTO bill_master VALUES
("1601","","","3","12","A","","","2016-06-29 04:39:47","04:39:47","0","L","07:22:38"),
("1603","","","4","14","A","","","2016-06-29 04:59:30","04:59:30","0","L","07:22:59"),
("1604","","","7","4","A","","","2016-06-29 05:03:25","05:03:25","0","C","07:23:39"),
("1605","","","1","1","A","","","2016-06-29 05:58:43","05:58:43","0","L","06:27:52"),
("1606","","","14","15","A","","","2016-06-29 07:23:08","07:23:08","0","L","07:23:08"),
("1607","T2","900000000","3","5","D","billtaken","","2016-06-30 10:14:24","10:14:24","70","L","03:49:10"),
("1608","T4","900000000","3","2","C","nottaken","","2016-06-30 10:57:20","10:57:20","340","C","10:57:20"),
("1609","T10","900000000","3","2","D","nottaken","","2016-06-30 03:49:53","03:49:53","280","L","03:49:57"),
("1610","","","5","4","","nottaken","","2016-06-30 00:00:00","00:00:00","470","L","00:00:00"),
("1611","T6","900000000","2","4","","nottaken","","2016-06-30 00:00:00","00:00:00","200","L","00:00:00"),
("1612","09","900000000","9","9","","nottaken","","2016-06-30 00:00:00","00:00:00","105","L","00:00:00"),
("1613","010","900000000","10","10","","nottaken","","2016-06-30 00:00:00","00:00:00","340","L","00:00:00"),
("1614","test","900000000","5","6","","nottaken","","2016-06-30 00:00:00","00:00:00","280","L","00:00:00"),
("1615","T7","900000000","3","2","","nottaken","","2016-06-30 00:00:00","00:00:00","90","L","00:00:00"),
("1616","T2","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","70","L","00:00:00"),
("1617","T3","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","195","L","00:00:00"),
("1618","T1","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","510","L","00:00:00"),
("1619","T2","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","70","L","00:00:00"),
("1620","T7","900000000","3","2","","nottaken","","2016-06-30 00:00:00","00:00:00","90","L","00:00:00"),
("1621","T1","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","510","L","00:00:00"),
("1622","T2","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","70","L","00:00:00"),
("1623","T2","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","70","L","00:00:00"),
("1624","T2","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","70","L","00:00:00"),
("1625","","","1","1","","nottaken","","2016-06-30 00:00:00","00:00:00","650","L","00:00:00"),
("1626","T6","900000000","2","4","","nottaken","","2016-06-30 00:00:00","00:00:00","200","L","00:00:00"),
("1627","08","900000000","8","8","","nottaken","","2016-06-30 00:00:00","00:00:00","120","L","00:00:00"),
("1628","T7","900000000","3","2","","nottaken","","2016-06-30 00:00:00","00:00:00","90","L","00:00:00"),
("1629","","","1","4","","nottaken","","2016-06-30 00:00:00","00:00:00","3780","L","00:00:00"),
("1630","","","1","4","","nottaken","","2016-06-30 00:00:00","00:00:00","3780","L","00:00:00"),
("1631","T4","900000000","3","2","","nottaken","","2016-06-30 00:00:00","00:00:00","340","L","00:00:00"),
("1632","T2","900000000","3","5","","nottaken","","2016-06-30 00:00:00","00:00:00","70","L","00:00:00");




CREATE TABLE `bill_master_original` (
  `billid` int(11) NOT NULL AUTO_INCREMENT,
  `bill_name` varchar(100) NOT NULL,
  `bill_mobile` varchar(50) NOT NULL,
  `bill_table` int(10) NOT NULL,
  `bill_waiter` int(10) NOT NULL,
  `bill_status` varchar(25) NOT NULL,
  `bill_notes` varchar(255) NOT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL,
  PRIMARY KEY (`billid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `billdetail` (
  `billdetailid` int(2) NOT NULL AUTO_INCREMENT,
  `billstart` int(10) NOT NULL,
  `vat` int(10) NOT NULL,
  `othertax` int(10) NOT NULL,
  `tinno` varchar(100) NOT NULL,
  `stno` varchar(100) NOT NULL,
  PRIMARY KEY (`billdetailid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


INSERT INTO billdetail VALUES
("1","1600","15","10","222222","666666");




CREATE TABLE `content` (
  `id` int(10) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `page_name` varchar(50) DEFAULT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `item_master` (
  `itemid` int(10) NOT NULL AUTO_INCREMENT,
  `itemname` varchar(100) NOT NULL,
  `kgPrice` varchar(255) NOT NULL,
  `price_type` varchar(255) NOT NULL,
  `itemSeq` varchar(255) NOT NULL,
  `backColor` varchar(255) NOT NULL,
  `visible` varchar(255) NOT NULL,
  `itemprice` float(10,2) NOT NULL,
  `itemnotes` varchar(255) NOT NULL,
  `item_image` varchar(100) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;


INSERT INTO item_master VALUES
("1","Vanilla","","","","","","40.00","Vanilla","1_download-1.jpg"),
("2","Kaju Draksh","","setItemId","","f9f0ac","","50.00","","2_vannila.jpg"),
("3","Shiv Special","","setItemId","","acf9e5","","70.00","",""),
("4","King Kone","","setItemId","","f9f0ac","","35.00","","4_thumb320110526075200.jpg"),
("5","Orange Candy","","setItemId","","f9baac ","","30.00","Orange Candy","5_green-food-grade-disposable-wooden-stick-ice.jpg"),
("6","Mava Candy","","setItemId","","acf9e5","","40.00","",""),
("7","Sundae","","setItemId","","f9f0ac","","130.00","",""),
("8","Mosambi Juice","","setItemId","","f9acda ","","60.00","","8_mosambijuice.jpg"),
("9","Faluda","","setItemId","","f9baac ","","90.00","","9_faluda.jpg"),
("10","Noodles","","setItemId","","f9acda ","","140.00","","10_veg-noodles.jpg"),
("11","Bhel","","setItemId","","f9f0ac","","140.00","",""),
("12","Manchurian","","setItemId","","acf9e5","","140.00","","12_maxresdefault.jpg"),
("13","jain bhel","","setItemId","","f9acda ","","30.00","","13_mens.jpg"),
("18","test","","setItemId","","f9baac ","","5.00","test","18_j2.jpg"),
("19","test","","setItemId","","acf9e5","","5.00","test","19_life-img2.jpg"),
("21","test","1","setItemId","2","f9baac ","","5555.00","wwwwwwwwwwwwwwwwwwwww","21_logo.png"),
("22","1test","5555","setIceCreamItemId","500","f9acda ","1","555.00","no note","22_life-img2.jpg"),
("25","test","","setItemId","","f9f0ac","","5555.00","1","25_life-img2.jpg"),
("26","wichtable","","setItemId","","f9acda ","","5555.00","no note","26_pthero42153645159.jpg"),
("27","vanila k","","setItemId","","acf9e5","","50.00","","27_1-boxx-lv-sofa-qtr-view-3.jpg"),
("28","vanila kg","500","setIceCreamItemId","1","f9f0ac","1","50.00","",""),
("29","vanila k","1","setIceCreamItemId","","","1","50.00","","29_1.png");




CREATE TABLE `siteadmin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `adminemail` varchar(50) NOT NULL,
  `user_type` enum('1','2') NOT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


INSERT INTO siteadmin VALUES
("1","admin","21232f297a57a5a743894a0e4a801fc3","shive admin","admin@gmail.com","1"),
("2","user","ee11cbb19052e40b07aac0ca060c23ee","","user@gmail.com","2");




CREATE TABLE `table_master` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `tname` varchar(20) NOT NULL,
  `floor` enum('0','1') NOT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;


INSERT INTO table_master VALUES
("3","1","0"),
("4","2","0"),
("5","3","0"),
("6","4","0"),
("7","5","0"),
("8","6","0"),
("9","7","0"),
("10","8","0"),
("11","9","0"),
("12","10","0"),
("13","11","0"),
("14","12","0"),
("15","13","0"),
("16","14","0"),
("17","15","0"),
("18","16","0"),
("19","17","0"),
("20","18","0"),
("21","19","0"),
("22","20","0"),
("23","51","1"),
("24","52","1"),
("25","53","1"),
("26","54","1"),
("27","55","1"),
("28","56","1"),
("29","57","1"),
("30","58","1"),
("31","59","1"),
("32","60","1");




CREATE TABLE `temp_bill_item_master` (
  `bim_id` int(10) NOT NULL AUTO_INCREMENT,
  `billid` int(10) NOT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` float(10,2) NOT NULL,
  PRIMARY KEY (`bim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=latin1;


INSERT INTO temp_bill_item_master VALUES
("4","3","4","3","35","105.00"),
("5","3","5","3","30","90.00"),
("6","4","10","2","140","280.00"),
("7","4","5","2","30","60.00"),
("8","5","7","2","130","260.00"),
("9","6","6","5","40","200.00"),
("10","7","5","3","30","90.00"),
("11","8","6","5","40","200.00"),
("12","8","2","2","50","100.00"),
("13","9","3","3","70","210.00"),
("14","10","12","2","140","280.00"),
("15","2","4","2","35","70.00"),
("16","11","7","2","130","260.00"),
("17","12","11","5","140","700.00"),
("18","13","11","1","140","140.00"),
("19","13","9","2","90","180.00"),
("20","13","2","3","50","150.00"),
("21","14","11","10","140","1400.00"),
("22","14","9","11","90","990.00"),
("23","14","2","12","50","600.00"),
("24","15","11","1","140","140.00"),
("25","16","11","2","140","280.00"),
("26","16","9","1","90","90.00"),
("27","17","11","3","140","420.00"),
("28","17","9","2","90","180.00"),
("29","17","2","1","50","50.00"),
("33","23","11","2","140","280.00"),
("34","24","6","3","40","120.00"),
("35","25","6","5","40","200.00"),
("36","26","4","3","35","105.00"),
("37","27","11","2","140","280.00"),
("38","27","5","2","30","60.00"),
("39","28","1","3","40","120.00"),
("40","28","3","3","70","210.00"),
("41","28","4","2","35","70.00"),
("42","29","11","2","140","280.00"),
("43","30","7","2","130","260.00"),
("44","31","11","1","140","140.00"),
("45","32","4","2","35","70.00"),
("46","33","5","3","30","90.00"),
("47","34","6","5","40","200.00"),
("48","35","1","3","40","120.00"),
("49","35","3","3","70","210.00"),
("50","35","4","2","35","70.00"),
("51","36","1","3","40","120.00"),
("52","36","3","3","70","210.00"),
("53","36","4","2","35","70.00"),
("54","37","11","2","140","280.00"),
("55","38","7","2","130","260.00"),
("56","39","4","2","35","70.00"),
("57","40","4","2","35","70.00"),
("58","41","4","3","35","105.00"),
("59","41","5","3","30","90.00"),
("60","42","5","3","30","90.00"),
("61","43","1","3","40","120.00"),
("62","43","3","3","70","210.00"),
("63","43","4","2","35","70.00"),
("64","44","11","2","140","280.00"),
("65","45","7","2","130","260.00"),
("66","46","4","2","35","70.00"),
("67","47","4","2","35","70.00"),
("68","48","4","3","35","105.00"),
("69","48","5","3","30","90.00"),
("70","49","5","3","30","90.00"),
("71","50","11","2","140","280.00"),
("72","50","2","5","50","250.00"),
("73","51","9","2","90","180.00"),
("74","51","8","10","60","600.00"),
("75","51","8","50","60","3000.00"),
("76","52","9","10","90","900.00"),
("77","52","2","20","50","1000.00"),
("78","52","9","30","90","2700.00"),
("87","53","5","3","30","90.00"),
("88","53","10","3","140","420.00"),
("89","54","9","2","90","180.00"),
("90","54","8","10","60","600.00"),
("91","54","8","50","60","3000.00"),
("92","55","4","2","35","70.00"),
("93","56","10","2","140","280.00"),
("94","56","5","2","30","60.00"),
("95","57","4","2","35","70.00"),
("96","58","11","3","140","420.00"),
("97","58","9","2","90","180.00"),
("98","58","2","1","50","50.00"),
("99","59","7","2","130","260.00"),
("100","60","6","5","40","200.00"),
("101","61","5","3","30","90.00"),
("102","62","5","3","30","90.00"),
("103","62","10","3","140","420.00"),
("104","63","9","2","90","180.00"),
("105","63","8","10","60","600.00"),
("106","63","8","50","60","3000.00"),
("107","64","4","2","35","70.00"),
("108","65","10","2","140","280.00"),
("109","65","5","2","30","60.00"),
("110","66","4","2","35","70.00"),
("111","67","11","3","140","420.00"),
("112","67","9","2","90","180.00"),
("113","67","2","1","50","50.00"),
("114","68","7","2","130","260.00");
INSERT INTO temp_bill_item_master VALUES
("115","69","6","5","40","200.00"),
("116","70","5","3","30","90.00"),
("117","71","5","3","30","90.00"),
("118","72","5","3","30","90.00"),
("119","72","10","3","140","420.00"),
("120","73","5","3","30","90.00"),
("121","73","10","3","140","420.00"),
("122","74","9","2","90","180.00"),
("123","74","8","10","60","600.00"),
("124","74","8","50","60","3000.00"),
("125","75","4","2","35","70.00"),
("126","76","10","2","140","280.00"),
("127","76","5","2","30","60.00"),
("128","77","4","2","35","70.00"),
("129","78","11","3","140","420.00"),
("130","78","9","2","90","180.00"),
("131","78","2","1","50","50.00"),
("132","79","7","2","130","260.00"),
("133","80","6","5","40","200.00"),
("134","81","5","3","30","90.00"),
("135","82","5","3","30","90.00"),
("136","82","10","3","140","420.00"),
("137","83","9","2","90","180.00"),
("138","83","8","10","60","600.00"),
("139","83","8","50","60","3000.00"),
("140","84","4","2","35","70.00"),
("141","85","10","2","140","280.00"),
("142","85","5","2","30","60.00"),
("143","86","4","2","35","70.00"),
("144","87","11","3","140","420.00"),
("145","87","9","2","90","180.00"),
("146","87","2","1","50","50.00"),
("147","88","7","2","130","260.00"),
("148","89","6","5","40","200.00"),
("149","90","5","3","30","90.00"),
("150","91","6","3","40","120.00"),
("151","92","9","2","90","180.00"),
("152","92","8","10","60","600.00"),
("153","92","8","50","60","3000.00"),
("154","93","5","3","30","90.00"),
("155","94","4","2","35","70.00"),
("156","95","5","3","30","90.00"),
("157","96","5","3","30","90.00"),
("158","97","11","5","140","700.00"),
("159","97","9","3","90","270.00"),
("160","97","4","3","35","105.00"),
("161","97","8","10","60","600.00"),
("162","98","6","3","40","120.00"),
("163","99","9","2","90","180.00"),
("164","99","8","10","60","600.00"),
("165","99","8","50","60","3000.00"),
("166","100","5","3","30","90.00"),
("167","101","4","2","35","70.00"),
("168","102","5","3","30","90.00"),
("169","103","5","3","30","90.00"),
("170","104","11","5","140","700.00"),
("171","104","9","3","90","270.00"),
("172","104","4","3","35","105.00"),
("173","104","8","10","60","600.00"),
("174","1","5","3","30","90.00"),
("175","1","10","3","140","420.00");




CREATE TABLE `temp_bill_master` (
  `billid` int(11) NOT NULL AUTO_INCREMENT,
  `bill_name` varchar(100) NOT NULL,
  `bill_mobile` varchar(50) NOT NULL,
  `bill_table` int(10) NOT NULL,
  `bill_waiter` int(10) NOT NULL,
  `bill_status` varchar(25) NOT NULL,
  `bill_notes` varchar(255) NOT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL,
  PRIMARY KEY (`billid`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;


INSERT INTO temp_bill_master VALUES
("1","T1","900000000","3","5","billtaken","","2016-04-27 17:12:30","17:12:30","510"),
("2","T2","900000000","3","5","nottaken","","2016-04-27 17:13:45","17:13:45","70"),
("3","T3","900000000","3","5","nottaken","","2016-04-27 17:14:26","17:14:26","195"),
("4","T4","900000000","3","2","nottaken","","2016-04-27 17:14:58","17:14:58","340"),
("5","T5","900000000","3","2","nottaken","","2016-04-27 17:15:26","17:15:26","260"),
("6","T6","900000000","2","4","nottaken","","2016-04-27 17:15:48","17:15:48","200"),
("7","T7","900000000","3","2","nottaken","","2016-04-27 17:16:47","17:16:47","90"),
("8","T8","910000000","3","2","nottaken","","2016-04-28 13:10:56","13:10:56","300"),
("9","T9","900000000","3","2","nottaken","","2016-04-28 13:11:22","13:11:22","210"),
("10","T10","900000000","3","2","nottaken","","2016-04-28 13:11:55","13:11:55","280"),
("11","T2","900000000","3","5","nottaken","","2016-04-27 00:00:00","17:13:45","70"),
("12","rakesh","7505000000","7","7","nottaken","","2016-05-05 09:17:34","13:17:34","700"),
("13","","","5","4","nottaken","","2016-05-08 23:14:55","03:14:55","470"),
("14","","","1","2","nottaken","","2016-05-08 23:15:59","03:15:59","2990"),
("15","","","1","1","nottaken","","2016-05-10 01:29:07","05:29:07","140"),
("16","","","1","1","nottaken","","2016-05-10 01:29:33","05:29:33","370"),
("17","","","1","1","nottaken","","2016-05-10 01:30:13","05:30:13","650"),
("23","002","900000000","5","6","nottaken","","2016-05-20 11:54:58","11:54:58","280"),
("24","08","900000000","8","8","nottaken","","2016-05-30 16:26:57","16:26:57","120"),
("25","T6","900000000","2","4","nottaken","","2016-05-20 00:00:00","00:00:00","200"),
("26","09","900000000","9","9","nottaken","","2016-05-30 16:27:20","16:27:20","105"),
("27","010","900000000","10","10","nottaken","","2016-05-30 16:28:01","16:28:01","340"),
("28","T2","900000000","3","5","nottaken","","2016-05-20 11:54:23","11:54:23","70"),
("29","test","900000000","5","6","nottaken","","2016-05-20 11:54:58","11:54:58","280"),
("30","T2","900000000","3","5","nottaken","","2016-05-20 11:55:23","11:55:23","70"),
("31","","","1","1","nottaken","","2016-05-20 00:00:00","00:00:00","140"),
("32","T2","900000000","3","5","nottaken","","2016-05-20 00:00:00","00:00:00","70"),
("33","T7","900000000","3","2","nottaken","","2016-05-20 00:00:00","00:00:00","90"),
("34","T6","900000000","2","4","nottaken","","2016-05-20 00:00:00","00:00:00","200"),
("35","T2","900000000","3","5","nottaken","","2016-05-20 11:54:23","11:54:23","70"),
("36","T2","900000000","3","5","nottaken","","2016-05-20 11:54:23","11:54:23","70"),
("37","test","900000000","5","6","nottaken","","2016-05-20 11:54:58","11:54:58","280"),
("38","T2","900000000","3","5","nottaken","","2016-05-20 11:55:23","11:55:23","70"),
("39","T2","900000000","3","5","nottaken","","2016-05-20 00:00:00","00:00:00","70"),
("40","T2","900000000","3","5","nottaken","","2016-05-20 00:00:00","00:00:00","70"),
("41","T3","900000000","3","5","nottaken","","2016-05-20 00:00:00","00:00:00","195"),
("42","T7","900000000","3","2","nottaken","","2016-05-20 00:00:00","00:00:00","90"),
("43","T2","900000000","3","5","nottaken","","2016-05-20 11:54:23","11:54:23","70"),
("44","test","900000000","5","6","nottaken","","2016-05-20 11:54:58","11:54:58","280"),
("45","T2","900000000","3","5","nottaken","","2016-05-20 11:55:23","11:55:23","70"),
("46","T2","900000000","3","5","nottaken","","2016-05-20 00:00:00","00:00:00","70"),
("47","T2","900000000","3","5","nottaken","","2016-05-20 00:00:00","00:00:00","70"),
("48","T3","900000000","3","5","nottaken","","2016-05-20 00:00:00","00:00:00","195"),
("49","T7","900000000","3","2","nottaken","","2016-05-20 00:00:00","00:00:00","90"),
("50","","","1","4","nottaken","","2016-06-21 10:46:52","10:46:52","530"),
("51","","","1","4","nottaken","","2016-06-21 10:48:02","10:48:02","3780"),
("52","","","1","4","nottaken","","2016-06-21 10:48:29","10:48:29","4600"),
("53","T1","900000000","3","5","nottaken","","2016-06-21 10:46:52","10:46:52","510"),
("54","","","1","4","nottaken","","2016-06-21 10:48:02","10:48:02","3780"),
("55","T2","900000000","3","5","nottaken","","2016-06-21 10:48:29","10:48:29","70"),
("56","T4","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","340"),
("57","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("58","","","1","1","nottaken","","2016-06-21 00:00:00","00:00:00","650"),
("59","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("60","T6","900000000","2","4","nottaken","","2016-06-21 00:00:00","00:00:00","200"),
("61","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("62","T1","900000000","3","5","nottaken","","2016-06-21 10:46:52","10:46:52","510"),
("63","","","1","4","nottaken","","2016-06-21 10:48:02","10:48:02","3780"),
("64","T2","900000000","3","5","nottaken","","2016-06-21 10:48:29","10:48:29","70"),
("65","T4","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","340"),
("66","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("67","","","1","1","nottaken","","2016-06-21 00:00:00","00:00:00","650"),
("68","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("69","T6","900000000","2","4","nottaken","","2016-06-21 00:00:00","00:00:00","200"),
("70","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("71","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("72","T1","900000000","3","5","nottaken","","2016-06-21 10:46:52","10:46:52","510"),
("73","T1","900000000","3","5","nottaken","","2016-06-21 10:46:52","10:46:52","510"),
("74","","","1","4","nottaken","","2016-06-21 10:48:02","10:48:02","3780"),
("75","T2","900000000","3","5","nottaken","","2016-06-21 10:48:29","10:48:29","70"),
("76","T4","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","340"),
("77","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("78","","","1","1","nottaken","","2016-06-21 00:00:00","00:00:00","650"),
("79","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("80","T6","900000000","2","4","nottaken","","2016-06-21 00:00:00","00:00:00","200"),
("81","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("82","T1","900000000","3","5","nottaken","","2016-06-21 10:46:52","10:46:52","510"),
("83","","","1","4","nottaken","","2016-06-21 10:48:02","10:48:02","3780"),
("84","T2","900000000","3","5","nottaken","","2016-06-21 10:48:29","10:48:29","70"),
("85","T4","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","340"),
("86","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("87","","","1","1","nottaken","","2016-06-21 00:00:00","00:00:00","650"),
("88","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("89","T6","900000000","2","4","nottaken","","2016-06-21 00:00:00","00:00:00","200"),
("90","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("91","08","900000000","8","8","nottaken","","2016-06-21 10:46:52","10:46:52","120"),
("92","","","1","4","nottaken","","2016-06-21 10:48:02","10:48:02","3780"),
("93","T7","900000000","3","2","nottaken","","2016-06-21 10:48:29","10:48:29","90"),
("94","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("95","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("96","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("97","","","1","4","nottaken","","2016-06-21 18:14:07","18:14:07","1675"),
("98","08","900000000","8","8","nottaken","","2016-06-21 10:46:52","10:46:52","120"),
("99","","","1","4","nottaken","","2016-06-21 10:48:02","10:48:02","3780"),
("100","T7","900000000","3","2","nottaken","","2016-06-21 10:48:29","10:48:29","90"),
("101","T2","900000000","3","5","nottaken","","2016-06-21 00:00:00","00:00:00","70"),
("102","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("103","T7","900000000","3","2","nottaken","","2016-06-21 00:00:00","00:00:00","90"),
("104","","","1","4","nottaken","","2016-06-21 18:14:07","18:14:07","1675");




CREATE TABLE `waiter_master` (
  `waiter_id` int(11) NOT NULL AUTO_INCREMENT,
  `waiter` varchar(20) NOT NULL,
  `floor` enum('0','1') NOT NULL,
  PRIMARY KEY (`waiter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;


INSERT INTO waiter_master VALUES
("1","T","0"),
("2","N","0"),
("3","G","0"),
("4","A","0"),
("5","J","0"),
("6","D","0"),
("7","V","0"),
("8","B","0"),
("9","F","0"),
("10","O","0"),
("11","M","0"),
("12","L","0"),
("13","U","0"),
("14","H","0"),
("15","K","0"),
("16","P","0"),
("17","FA","1"),
("18","FB","1"),
("19","FC","1"),
("20","FD","1"),
("21","FE","1"),
("22","FF","1"),
("23","FG","1"),
("24","FH","1"),
("25","FI","1"),
("26","FJ","1");


