<?php 
$check_session = 'no';
include_once("includes/connection_main.php");
$startdate = $_POST['fromdate'];
$enddate = $_POST['todate'];

?>
<thead>
	<tr role="row" align="center" class="text_center">
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Item id </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Item Name </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Quantity </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Weight </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Price (per Item) </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Total Billed Amount </th>
	</tr>
	<?php 
		$selqryweight = "SELECT weight FROM `bill_item_master` group by weight";
		$selectWeightRes = mysql_query($selqryweight) or die(mysql_error());
		$total_qty = 0;	
		$total_amt = 0;
		while ($selectWeightRow = mysql_fetch_array($selectWeightRes)) {
			if ($selectWeightRow['weight'] == '0') {
				$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right"><?php echo $selectWeightRow['weight']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
				}
			}
			elseif($selectWeightRow['weight'] == '250')
			{
				$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
						
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right">250 Gm</td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
				}	
			}
			elseif($selectWeightRow['weight'] == '500')
			{
				$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
						
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right">500 Gm</td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
				}
			}
			elseif($selectWeightRow['weight'] == '750')
			{
				$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
						
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right">750 Gm</td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
				}
			}
			elseif($selectWeightRow['weight'] == '1000')
			{
				$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
						
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right">1 Kg</td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
				}
			}
		}
	?>
	<tr class="gradeX odd" role="row">
		<td align="right"></td>
		<td>Total</td>
		<td align="right"><?php echo $total_qty; ?></td>
		<td align="right"></td>
		<td align="right"></td>
		<td align="right"><?php echo $total_amt; ?></td>
	</tr>     
</thead>