<?php
	$filename = "Add new bill";
	
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");

?>
  

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?=$filename?>
            <small>it all starts here</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?=$filename?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Bill Details</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
             
					<!--Add_Item_ST-->
					<div class="col-md-9">
					<form role="form" class="form-horizontal"  action="bill_db.php" name="frm_additem" id="frm_additem" method="post" >
					
					<input type="hidden" name="myaction" id="myaction"  value="additem" />
					<input type="hidden" name="itemid" id="itemid"  value="" />
					
					<div class="box-body">
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Customer Name</label>
						<div class="col-sm-9">
								<input type="text" placeholder="Enter customer Name" id="bill_name" name="bill_name" class="form-control" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Customer Mobile</label>
						<div class="col-sm-9">
							<input type="text" placeholder="Enter customer mobile" id="bill_mobile" name="bill_mobile" class="form-control" >
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Table Number</label>
						<div class="col-sm-9">
							<input type="text" placeholder="Enter table number" id="bill_table" name="bill_table" class="form-control" required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Waiter</label>
						<div class="col-sm-9">
							<input type="text" placeholder="Enter waiter details" id="bill_waiter" name="bill_waiter" class="form-control" required>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Bill Status</label>
						<div class="col-sm-9">
							<select name="bill_status" id="bill_status" required class="form-control">
								<option value="">Select Status</option>
								<option value="billtaken" >Bill Taken Home</option>
								<option value="nottaken" >Bill Not Taken Home</option>
							</select>
						</div>
					</div>
				
					
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Notes</label>
							<div class="col-sm-9">
							<input type="text" placeholder="Any Note" id="itemnotes" name="itemnotes" class="form-control" >
						</div>
					</div>
					
					<?php /*?>
					<div class="form-group">
					  <label  class="col-sm-3  control-label" for="inputEmail3">Time picker:</label>
				
						 <div class="input-group">
						  <div class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						  </div>
						  <input type="text"  id="timepicker" name="timepicker" class="form-control pull-right">
						</div>

						
					  <!-- /.input group -->
				</div>
					<?php */?>
					
					<br/>
					<div class="clear">&nbsp;</div>
			
					<div id="item_div">		
						<div class="input_fields_wrap">
						
							<!--Item_ST-->
							<div class="form-group">
								<label class="col-sm-3 control-label" for="inputEmail3">Item</label>
								<div class="col-sm-4">
										<?php  get_items();  ?>
							</div>
							<div class="col-sm-3">
								&nbsp;&nbsp;<strong>QNT </strong><input type="text" name="qnt[]" id="qnt" style="width:70px;" required  />
							</div>
						</div>
							<!--Item_ST-->

					</div>

					
							<div class="col-sm-12">
							<div class="col-sm-3">&nbsp;</div>
							<button class="btn btn-success add_field_button">Add More Fields &nbsp; <strong>+</strong> </button>
							</div>	
					
					
					</div>		
					
					</div>
					<!-- /.box-body -->
					
					<div class="col-sm-12" style="margin-top:50px;">
					<div class="col-sm-3">&nbsp;</div>
					<button class="btn btn-primary" type="submit">Create New Bill</button>
					&nbsp;&nbsp;
					<button onclick="location.href = 'dashbord.php';" class="btn btn-default" type="button">Cancel</button>
					</div>
					</form>
					<div class="clear">&nbsp;&nbsp;</div>
					</div>
					<!--Add_Item_EN-->
			 
            </div><!-- /.box-body -->
            <div class="box-footer">
              
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div>
	  <!-- /.content-wrapper -->

<script type="text/javascript">

$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
			
			
				// AJAX ST
					$.ajax({
						dataType: 'json',
						type: 'POST',
						url: 'ajax_add_bill_item.php',
						data: $(this).serialize(),
						//data: data_send.serialize()
						success:function(data) 
						{
							  //handleData(data); 
							  console.log("1");	
						  	 //$(wrapper).append('<div >'+data['msg']+'<a href="#" class="remove_field col-sm-3">Remove</a></div>'); //add	
							 $(wrapper).append('<div >'+data['msg']+'</div>'); //add	
						}
					
					})
					
					//$(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
	
});



$( document ).ready(function() {
		$('#timepicker').timepicki({	
				reset: true 
		});
});
</script>

 <?php
	include_once("includes/footer.php");
?>
      