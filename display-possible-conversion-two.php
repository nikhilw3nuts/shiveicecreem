<?php
	$filename = "Display Possible Cconversion";
	
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");

	/*
		printit();
		exit;
	*/
	
?>
<!-- Content Wrapper. Contains page content -->
<?php /*?>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<?php */?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?=$filename?>
      <small>it all starts here</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">
        <?=$filename?>
      </li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <?=$filename?>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!--ST-->
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Date picker</h3>
          </div>
          <div class="box-body" >
            <!-- Date -->
              <?php
				// printit();
				// exit;
			  ?>
            <form role="form" class="form-horizontal"  action="display-possible-conversion.php" name="frm_additem" id="frm_additem" method="post" >
              <input type="hidden" name="myaction" id="myaction"  value="additem" />
              <input type="hidden" name="itemid" id="itemid"  value="" />
              <div class="form-group">
                <div class="col-sm-3" align="right">
                  <label class="control-label">Date:</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input type="text" id="datepicker" name="datepicker" value="<?php echo $_REQUEST['datepicker']; ?>" class="form-control pull-right">
                  </div>
                </div>
                <div class="col-sm-3">
                  <button class="btn btn-primary" type="submit">Show Bill</button>
                </div>
                <!-- /.input group -->
              </div>
            </form>
            
            <div class="form-group">
            <?php
				$is_max_date = 0;
				if(isset($_REQUEST['datepicker']) && $_REQUEST['datepicker'] != ""){
					$last_order_date = max_order_date(); // Max last date ********
					
					if($_REQUEST['datepicker'] == $last_order_date){
						$is_max_date = 1;	// search date and maturity date are same so now allow customer for more transfer
					}
				}
			
				/*	
				echo '<hr>';
				echo $last_order_date.'---- '.$is_max_date;	// is max date
				echo '<hr>';
				*/
				
				// nottaken   and   billtaken
				if($last_order_date != "" && $is_max_date == "1"){
					
					$last_order_date_mysql = date("Y-m-d",strtotime($last_order_date));
					
					$total_orders = select_total_orders($last_order_date_mysql);
					$hometaken_orders = select_hometaken_orders($last_order_date_mysql);
					$not_hometaken_orders = select_not_hometaken_orders($last_order_date_mysql);
					
					$min_required_not_hometaken = "0";
					if($hometaken_orders > 0){
						$max_home_taken_order_id = max_home_taken_order_id($last_order_date_mysql);
						$min_required_not_hometaken = select_min_required_not_home_taken($last_order_date_mysql,$max_home_taken_order_id);
					}else{
					
					}
					
					/*
						echo "<br/> total orders **** ".$total_orders;
						echo "<br/> total home taken orders **** ".$hometaken_orders;
						echo "<br/> total not home taken orders **** ".$not_hometaken_orders;
						echo "<br/> total min required homenot taken **** ".$min_required_not_hometaken;					
					*/
					
				}
				
				
			?>
            </div>
            
		    <div id="show_conv_amount"  style="margin-top:50px;"> 
			<form role="form" class="form-horizontal"  action="display-possible-conversion-two.php" name="form_amount_percent" id="form_amount_percent" method="post" >
              <input type="hidden" name="myaction" id="myaction"  value="additem" />
              <input type="hidden" name="itemid" id="itemid"  value="" />
			  <input type="hidden"  id="datepicker" name="datepicker" value="<?php echo $_REQUEST['datepicker']; ?>"  />
              <div class="form-group">
                <div class="col-sm-3" align="right">
                  <label class="control-label">Current Amount :</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></div>
                    <input type="text" readonly id="current_amount" name="current_amount"  value="<?php echo $_REQUEST['current_amount']; ?>"  class="form-control pull-right">
                  </div>
                </div>
               
                <!-- /.input group -->
              </div>
			  
			  <div class="form-group">
               <?php
				  	//printit();
				  ?>
                <div class="col-sm-3" align="right">
                  <label class="control-label">Make :</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-percent" aria-hidden="true"></i></div>
                    <input type="text" readonly id="amount_percent" name="amount_percent" class="form-control pull-right"  value="<?php echo $_REQUEST['amount_percent']; ?>" >
                  </div>
                 
                </div>
               
                <!-- /.input group -->
              </div>
			  
			  <div class="form-group">
              
                <div class="col-sm-3" align="right">
                  <label class="control-label">Expected Amount :</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></div>
                    <input type="text" readonly id="amount_val" name="amount_val"  class="form-control pull-right"  value="<?php echo $_REQUEST['amount_val']; ?>"  required >
                  </div>
                </div>
				
               </div> 
	       
            <?php if(isset($_REQUEST['taken_home']) && count($_REQUEST['taken_home']) > 0){ ?>
            <div class="form-group">
            	<div class="col-sm-3" align="right">
                  <label class="control-label">Home Taken :</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></div>
                    <input type="text" readonly id="amount_val" name="amount_val"  class="form-control pull-right"  value="<?php echo array_sum($_REQUEST['taken_home']); ?>"  required >
                  </div>
                </div>
            </div>
           <?php } ?>
            
            <?php if(isset($_REQUEST['taken_home']) && count($_REQUEST['taken_home']) > 0){ 
				$taken_home_total = array_sum($_REQUEST['taken_home']);
				$remaining_amount = $_REQUEST['amount_val'] - $taken_home_total;
			?>
            	<div class="form-group">
            	<div class="col-sm-3" align="right">
                  <label class="control-label">Remaining :</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon" style="background-color:#00A65A; color:#F7F5F5;"><i class="fa fa-inr" aria-hidden="true"></i></div>
                    <input type="text" readonly id="amount_val" name="amount_val"  class="form-control pull-right"  value="<?php echo $remaining_amount; ?>"  required >
                  </div>
                </div>
            </div>
            <?php }else{
					$remaining_amount = $_REQUEST['total_remaining_final'];
			 } ?>
            
            <br/><br/><br/>
              
            </form>
			</div>
					
            <!--Listing_ST-->
            <?php
			if(isset($_REQUEST['datepicker']) && $_REQUEST['datepicker'] != "" ){ 
					$datepicker = isset($_REQUEST["datepicker"])?strtolower(trim($_REQUEST["datepicker"])):"";
			?>
			
            <div class="table-scrollable">
             
               <div class="alert alert-info alert-dismissible bg-green disabled color-palette" style="padding:10px 15px;">
						<h4>
							<i class="icon fa fa-warning"></i> STEP-2 &nbsp;&nbsp;
							<span>These are the Template orders which can be used</span>
						</h4>
						Note : Drage and Drop order from one table to another table
				</div>
				 
				 <!--First_Table_ST-->
				 <?php
				 		$records_array = orders_records_array($remaining_amount);
						//print_r($records_array);
						$records_array_list = implode(",",$records_array);
						//exit;
						$amount_val = $remaining_amount;
					
						$sel_data_qry = "SELECT 
						t1.*, 
						( SELECT sum(bill_total) FROM temp_bill_master WHERE billid <= t1.billid and bill_total <= '".$amount_val."' ORDER BY `bill_total` DESC ) as 'final_total_bill' 
						FROM temp_bill_master as t1 where bill_total <=  '".$amount_val."' 
						HAVING final_total_bill <= '".$amount_val."' 
						ORDER BY `final_total_bill` ASC";
						
						$sel_data_qry = "SELECT * from temp_bill_master WHERE billid IN (".$records_array_list.") ";
						

						if ($ins_qry_data_res = mysql_query($sel_data_qry))
						{ 	// echo "New record created successfully";
							$num_rows = mysql_num_rows($ins_qry_data_res);
							
						}else{ 
							// echo "Error: <br/>" . $sel_data_qry . "<br>" . mysql_error();  
						}
				 ?>	
				 <div class="col-sm-6 bitacoratable"  id="table1"  >
				 <form action="" method="post" name="frm1" id="frm1">

                 		<input type="hidden" name="total_oders" id="total_oders" value="<?php echo $total_orders; ?>" >
                  		<input type="hidden" name="hometaken_orders" id="hometaken_orders" value="<?php echo $hometaken_orders; ?>" >
                  		<input type="hidden" name="not_hometaken_orders" id="not_hometaken_orders" value="<?php echo $not_hometaken_orders; ?>" >
                  		<input type="hidden" name="min_required_not_hometaken" id="not_hometaken_orders" value="<?php echo $min_required_not_hometaken; ?>" >
                 
                 		<input type="hidden" name="is_max_date" id="is_max_date" value="<?php echo $is_max_date; ?>" >

					 	<?php foreach($_REQUEST['order_list'] as $okey => $oval){ //echo $oval." ****v*"; ?>
							<input type="hidden" id="order_list" name="order_list[]" value="<?php echo $oval; ?>"  />
						<?php } ?>	
						  <input type="hidden"  id="datepicker" name="datepicker" value="<?php echo $_REQUEST['datepicker']; ?>"  />

<div class="final-table">
				    <table  class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" ><tr class="childrow"><td collspan = "1" >
					<div class="box-header" align="center">
						  <h3 class="box-title"><i class="fa fa-fw fa-check-square-o"></i>Final orders which will be used</h3>
					</div>
						<table id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer childgrid" role="grid" aria-describedby="sample_1_info">
						  <thead>
							<tr role="row" align="center" class="text_center btn-warning">
							  <th rowspan="1" colspan="1" style="width: 68px;" aria-label=""> </th>
							  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Time</th>
							  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill Total </th>
							  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> View </th>
							  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> Delete </th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							if($num_rows > 0){
								$total_bill_id = array();
								$total_bils = array();
								while($info = mysql_fetch_assoc($ins_qry_data_res)){
									$total_bill_id[] = $info['billid'];
									
									$cnt++;							
									/*
										echo '<pre>';
										print_r($info);
										echo '</pre>';						
									*/
						 ?>
						<tr class="gradeX odd  draggable_tr" role="row" id="input_fields_wrap">
						  <input type="hidden" id="order_list_dup" name="order_list_dup[]" value="<?php echo $info['billid']; ?>"  />
						  <td><?php //echo $cnt; //$info['aid']; ?><?php echo $info['billid']; ?></td>
						  <td class="text_center"><?php 	echo $mysqldate =  date('h:i A', strtotime($info['bill_date']));    ?>
						  </td>
						 
						  <td align="right"><?php 
													$total_bils[] = $info['billid'];
													echo $total_bill =  get_temp_bill_total($info['billid']);
													?>
													<input type="hidden" name="hidden_bill_total[]" value="<?php echo $total_bill;?>"		 />
													<input type="hidden" name="hidden_bill_id[]" value="<?php echo $info['billid']; ?>"	  />

						  </td>
						  <td align="center"><a target="_blank" href="temp_edit_bill.php?b=<?php echo $info['billid'] ?>" style="text-decoration:none;"> <span class="label label-sm label-success"> <strong>View </strong></span> </a> </td>
						  <td align="center">
						 	<button type="button" class="removebutton  btn-instagram" title="Remove this row"><strong>Remove</strong></button>
						  
						   </td>
						</tr>
						<?php
							}
						?>
							 </tbody>
				</table>

					<?php
												
							}else{
							
							echo '<table  class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer  childgrid ui-droppable">
							<tr role="row" align="center" class="text_center">';
								echo '<td colspan="8">';
									echo 'No Records Found!';
								echo '</td>';
							echo '</tr></table>';
							
							}
							?>
						<input type="hidden" name="myaction">
						<input name="billid" type="hidden" id="billid">
						<input type="hidden" name="sorton" value="<?=$sorton?>">
						<input type="hidden" name="sort" value="<?=$sort?>">
					  </form>
					  <?php
					  		if($total_bill_id != ""){
									$total_bill_id_array = implode("', '", array_values($total_bill_id));
							}		
					  ?>
					  
					  </td></tr></table>
					  </div>
					  
					  <table  class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer  childgrid ui-droppable">
						<tr role="row" align="center" class="text_center">
								 		<td   align="center" >Home Taken  &nbsp;
							 			<strong><span id="frm_hometaken_tot" ><?php echo array_sum($_REQUEST['taken_home']); ?></span></strong>
										</td>

								 		<td   align="center" >Calculated Amount &nbsp;
							 			<strong><span id="frm_one_tot" ><?php if($total_bils != ""){ echo $total_bills_amount = total_temp_bills($total_bils);  }?></span></strong>
										</td>

							<td   align="center" >Total Amount &nbsp;
							<strong><span id="frm_all_total" ><?php if($total_bils != ""){ echo $total_bills_amount = total_temp_bills($total_bils)+array_sum($_REQUEST['taken_home']);  }?></span></strong>
							</td>
						</tr>
					  </tbody>
				</table>
					  
				</div>
				 <!--First_Table_EN-->
				
				<!--Seconds_ST-->	
				  <?php
				// **************** MyCondition ST ***********************
					$myCondition = Array();
					$total_bills_amount = "";
						
					array_push($myCondition," billid NOT IN ('".$total_bill_id_array."') ");	
					
					if(count($myCondition) > 0)
						$myCondition = " WHERE ".implode(" AND ", $myCondition);
					else
						$myCondition = "";
					
				// **************** MyCondition EN ***********************
				
				$sel_data_qry = "SELECT  * FROM temp_bill_master ".$myCondition." ";

				if ($ins_qry_data_res = mysql_query($sel_data_qry))
				{ 	// echo "New record created successfully";
					$num_rows = mysql_num_rows($ins_qry_data_res);
					
				}else{  echo "Error: " . $sel_qry . "<br>" . mysql_error($db);  }
		 ?>	
		 <div class="col-sm-6 bitacoratable"  id="table1"  >
		 <div class="temparary-table">
 	     <table  class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" ><tr class="childrow"><td collspan = "1" >
		 
		 <div class="box-header" align="center">
			  <h3 class="box-title"><i class="fa fa-fw fa-plus-square-o"></i>Temparary orders which can be used</h3>
		</div>
		  <form action="" method="post" name="frm2" id="frm2">
				<table id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer  childgrid" role="grid" aria-describedby="sample_1_info">
				  <thead>
					<tr role="row" align="center" class="text_center btn-twitter">
					  <th rowspan="1" colspan="1" style="width: 68px;" aria-label=""> </th>
					  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Time</th>
					  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill Total </th>
					  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> View</th>
					  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> Delete</th>
					</tr>
				  </thead>
				  <tbody>
					<?php
					if($num_rows > 0){
							$total_bill_id = array();
							$total_bils = array();
							while($info = mysql_fetch_assoc($ins_qry_data_res)){
											$total_bill_id[] = $info['billid'];
											
											$cnt++;							
											/*
											echo '<pre>';
											print_r($info);
											echo '</pre>';						
											*/
											?>
				<tr class="gradeX odd draggable_tr" role="row" id="input_fields_wrap">
					<input type="hidden" id="order_list_dup" name="order_list_dup[]" value="<?php echo $info['billid']; ?>"  />
				  <td><?php // echo $cnt; //$info['aid']; ?><?php echo $info['billid']; ?></td>
				  <td class="text_center"><?php 	echo $mysqldate =  date('h:i A', strtotime($info['bill_date']));    ?>
				  </td>
				 
				  <td align="right"><?php 
											$total_bils[] = $info['billid'];
											echo $total_bill =  get_temp_bill_total($info['billid']);
											?>
								<input type="hidden" name="hidden_bill_total[]" value="<?php echo $total_bill;?>"		 />
								<input type="hidden" name="hidden_bill_id[]" value="<?php echo $info['billid']; ?>"	  />
				  </td>
				   <td align="center"><a target="_blank" href="temp_edit_bill.php?b=<?php echo $info['billid'] ?>" style="text-decoration:none;"> <span class="label label-sm label-success"> <strong>View </strong></span> </a> </td>
				  <td align="center">
						
				 <button type="button" class="removebutton  btn-instagram" data-bill="<?php echo $total_bill;?>" title="Remove this row"><strong>Remove</strong></button>
				  
				   </td>
				</tr>
				<?php
					}
				?>
				  </tbody>
				</table>

					<?php
												
							}else{
							
							echo '<table  class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer  childgrid ui-droppable">
							<tr role="row" align="center" class="text_center">';
								echo '<td colspan="8">';
									echo 'No Records Found!';
								echo '</td>';
							echo '</tr></table>';
							
							}
							?>
				
					<input type="hidden" name="myaction">
					<input name="billid" type="hidden" id="billid">
					<input type="hidden" name="sorton" value="<?=$sorton?>">
					<input type="hidden" name="sort" value="<?=$sort?>">
					

			  </form>
			  <?php
					$total_bill_id_array = implode("', '", array_values($total_bill_id));
			  ?>
			  </td></tr></table> 
			  </div>
			
			    <table  class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer  childgrid ui-droppable" style="display:none;">
							<tr role="row" align="center" class="text_center">
									<td   align="center" >Total  &nbsp;
											<strong><span id="frm_two_tot" >
											<?php if($num_rows > 0){ ?>
											<?php if($total_bils != ""){ echo $total_bills_amount = total_temp_bills($total_bils);  } ?></span></strong>
											<?php }else{ echo '0'; } ?>
									</td>
							</tr>
					  </tbody>
				</table>
					  
			</div>
		<!--Seconds_EN-->	
					
    </div>
			
			
			
			<!--All_Temparary_Records_ST-->
			<!--All_Temparary_Records_EN-->

			
            <?php
				} // End isset date picker 

			  ?>
            <!--Listing_EN-->
			
            <div class="col-md-12" align="center" style="margin:25px auto;">
            <div style="width:300px;">
                    <button class="btn btn-block btn-social btn-bitbucket" type="button"  id="final_submit">
                    <i class="fa fa-fw fa-random"></i>
                   Update Selected Records 
                </button>
                </div>
            </div>

          </div>
        </div>
      </div>
	   
	   
      <!--EN-->
      <!-- /.box-body -->
      <div class="box-footer"> </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
	include_once("includes/footer.php");
?>
<script>
	$( document ).ready(function() {
  	
				$('#datepicker').datepicker({
					format: 'dd-mm-yyyy',
					autoclose: true,
					todayHighlight: true
				});

				 
				 // Remove Table TR ST --------------------------
				  $(document).on('click', 'button.removebutton', function () {
				  
				  		 $(this).closest('tr').remove();

						   // Ajax update total  FRM 1 ST ----------------------------------
								$.ajax({
									dataType: 'json',
									type: 'POST',
									url: 'ajax_update_total.php',
									data: $('#frm1').serialize(),
										success:function(data) 
										{
											 console.log(data);	
											  $('#frm_one_tot').html(data['frm_one_total']);
											  $('#frm_all_total').html(parseFloat($('#frm_one_tot').html())+parseFloat($('#frm_hometaken_tot').html()));
										}
								
								});
							  // Ajax update total  FRM 1 EN ----------------------------------
							  
								 // Ajax update total  FRM 1 ST ----------------------------------
								$.ajax({
									dataType: 'json',
									type: 'POST',
									url: ' ajax_update_total_frm_two.php',
									data: $('#frm2').serialize(),
										success:function(data) 
										{
											 console.log(data);	
											 $('#frm_two_tot').html(data['frm_two_total']);
											 $('#frm_all_total').html(parseFloat($('#frm_one_tot').html())+parseFloat($('#frm_hometaken_tot').html()));
										}
								
								});
							  // Ajax update total  FRM 1 EN ----------------------------------
							  
							  

						 return false;
				 }); 
				 // Remove Table TR EN --------------------------

			
					// $('#current_amount').val(<?php echo $total_bills_amount; ?> )

					///////
  
				  $("#amount_percent").change(function() { 
						//var $this = $(this);
						
						//alert($('#amount_percent').val());	
						
						 // Ajaxy ST	 ---------------			
						 $.ajax({
							dataType: 'json',
							type: 'POST',
							url: 'ajax_amount_percent_value.php',
							data: $('#form_amount_percent').serialize(),
							success:function(data) 
							{
								  	//console.log(data);	
									$("#amount_val").val(data['exp_amt']);
							}
						
						})
						 // Ajaxy EN	 --------------
														
				  });
				  
		
  });
</script>

<!--Drage_ST-->
<style type="text/css">
.childgrid tr {  cursor:move; }
</style>

<script type="text/javascript">
$("#table1 .childgrid tr, #table2 .childgrid tr").draggable({
        helper: function(){
        var selected = $('.childgrid tr.selectedRow');
        
		if (selected.length === 0) {
          selected = $(this).addClass('selectedRow');
        }
        var container = $('<div/>').attr('id', 'draggingContainer');
    container.append(selected.clone().removeClass("selectedRow"));
    return container;
      }
 });

$("#table1 .childgrid, #table2 .childgrid").droppable({

    drop: function (event, ui) {
  		  $(this).append(ui.helper.children());
 	  	  $('.selectedRow').remove();
		  
		  
		    // Ajax update total  FRM 1 ST ----------------------------------
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: 'ajax_update_total.php',
				data: $('#frm1').serialize(),
					success:function(data) 
					{
						 console.log(data);	
						  $('#frm_one_tot').html(data['frm_one_total']);
						  alert("Add/Remove Bill successfully");
						  $('#frm_all_total').html(parseFloat(data['frm_one_total'])+parseFloat($("#frm_hometaken_tot").html()))
					}
			
			});
		  // Ajax update total  FRM 1 EN ----------------------------------
		  
		     // Ajax update total  FRM 1 ST ----------------------------------
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: ' ajax_update_total_frm_two.php',
				data: $('#frm2').serialize(),
					success:function(data) 
					{
						 console.log(data);	
						 $('#frm_two_tot').html(data['frm_two_total']);
						 $('#frm_all_total').html(parseFloat(data['frm_two_total'])+parseFloat($("#frm_hometaken_tot").html()))
					}
			
			});
		  // Ajax update total  FRM 1 EN ----------------------------------
		  
		 
		  
    }

});

$(document).on("click", ".childgrid tr", function () {
    $(this).toggleClass("selectedRow");
});

$( "#draggable" ).draggable();

$( document ).ready(function() {

		//var hiddnelen = $("#order_list").length;
		var order_list_len = $('input[name="order_list[]"]').length
		console.log(order_list_len);
		
  		$("#final_submit").click(function(){

				var order_list_len = $('input[name="order_list[]"]').length
				//console.log(order_list_len);

				var order_list_dup_len = $('#frm1 input[name="order_list_dup[]"]').length
				//console.log(order_list_dup_len);
		
				console.log(order_list_len+" *** "+order_list_dup_len);
				if(order_list_len == "0")
				{
					swal({
						  title: "Orders Mismatch!",
						  text: "You Need atleast 1 original order.<br/> Please go back and select orders.",
						  html: true,
						  type: "error",
						   
						});	
					//swal("Orders Mismatch!", "You Need atleast 1 original order.<br/> Please go back and select orders. ", "error");	
				
				}else if( order_list_len != order_list_dup_len ){	

					
					 // 1. order_list_len != order_list_dup_len  ::: If Ordiginal orders number are not equal to duplicate orders
					 // 2. if max_orderdate == 1 ( means last date selected ) 
					 var max_oderdate = "";
					 max_oderdate = "<?php echo $is_max_date; ?>";
					 
					 var total_orders_js = "";
					 total_orders_js = "<?php echo $total_orders; ?>";

					 var hometaken_orders_js = "";
					 hometaken_orders_js = "<?php echo $hometaken_orders; ?>";

					 var not_hometaken_orders_js = "";
					 not_hometaken_orders_js = "<?php echo $not_hometaken_orders; ?>";

					 var min_required_not_hometaken_js = "";
					 min_required_not_hometaken_js = "<?php echo $min_required_not_hometaken; ?>";

					 
					 //max_oderdate = 1;
					 if( (typeof max_oderdate != "undefined" ) && max_oderdate != "" && max_oderdate == 1 ){

						// when max_orderdate == 1 then allow more duplicate records
						
						if(min_required_not_hometaken_js != 0){
							
							if( order_list_dup_len < min_required_not_hometaken_js )
							{
								// when minimum required orders are less then required 
								swal("Orders Mismatch!", "It's Last order date! You Need atlease "+min_required_not_hometaken_js+" orders, Currently its "+order_list_dup_len+" ", "warning");	
								
							}else if( order_list_dup_len >= min_required_not_hometaken_js ){
								
								// when minimum required orders are more
									swal({
										  title: "Are you sure with changes ?",
										  //text: "It's last order date <?php echo $_REQUEST['datepicker']; ?> <br/> Total <?php echo $total_orders;?> orders.<br/>Total home taken <?php echo $hometaken_orders?> bill.<br/>Total not home taken <?php echo $not_hometaken_orders?> bill.<br/><br/>You are replacing "+order_list_dup_len+" orders. <br/>You need minimum "+min_required_not_hometaken_js+" orders.",
										  text: "<table width='227px' border='1' align='center'><th></th><th>New</th><th>Old</th><tr><td>Total</td><td align='right'>"+order_list_dup_len+"</td><td align='right'><?php echo $total_orders;?></td></tr><tr><td>Home</td><td align='right'><?php echo $hometaken_orders?></td><td align='right'><?php echo $hometaken_orders?></td></tr><tr><td>Not H.</td><td align='right'>"+order_list_dup_len+"</td><td align='right'><?php echo $total_orders+$hometaken_orders;?> </td></tr></table>",
										  type: "info",
										  showCancelButton: true,
										  closeOnConfirm: false,
										  confirmButtonText: "Yes, update it!",
										  showLoaderOnConfirm: true,
										  html: true,
									},
									function(){
										setTimeout(function(){
											document.frm1.action="final_replacement_updated.php";
											document.frm1.submit();
									  }, 2000);
									});

							}
							
						}else{

							// When min home taken requires 0
								swal({
									  title: "Are you sure with changes ?",
									  //text: "It's last order date <?php echo $_REQUEST['datepicker']; ?> <br/> Total <?php echo $total_orders;?> orders.<br/>Total home taken <?php echo $hometaken_orders?> bill.<br/>Total not home taken <?php echo $not_hometaken_orders?> bill.<br/><br/>You are replacing "+order_list_dup_len+" orders. <br/>You need minimum "+min_required_not_hometaken_js+" orders.",
									  text: "<table width='227px' border='1' align='center'><th></th><th>New</th><th>Old</th><tr><td>Total</td><td align='right'>"+order_list_dup_len+"</td><td align='right'><?php echo $total_orders;?></td></tr><tr><td>Home</td><td align='right'><?php echo $hometaken_orders?></td><td align='right'><?php echo $hometaken_orders?></td></tr><tr><td>Not H.</td><td align='right'>"+order_list_dup_len+"</td><td align='right'><?php echo $total_orders+$hometaken_orders;?> </td></tr></table><br/>You need minimum "+min_required_not_hometaken_js+" orders.",
									  type: "info",
									  showCancelButton: true,
									  closeOnConfirm: false,
									  confirmButtonText: "Yes, update it!",
									  showLoaderOnConfirm: true,
									  html: true,
								},
								function(){
									setTimeout(function(){
										document.frm1.action="final_replacement_updated.php";
										document.frm1.submit();
								  }, 2000);
								});
						}
						
						
					 }else{
						 
						 // when not max order then original order and duplicate order should be same
						 swal("Orders Mismatch!", "You Need "+ order_list_len+" orders, Currently its "+order_list_dup_len+" ", "warning");
					 }
					 
					// order_list_len != order_list_dup_len : main order and duplicate orders are mismatch 
					// var max_oder_date = php max order date
					
					// swal("Orders Mismatch!", "You Need "+ order_list_len+" orders, Currently its "+order_list_dup_len+" ", "warning");

				}else{
					
					swal({
						  title: "Are you sure with changes ?",
						  text: "",
						  type: "info",
						  showCancelButton: true,
						  closeOnConfirm: false,
						  confirmButtonText: "Yes, update it!",
						  showLoaderOnConfirm: true,
					},
					function(){
					  setTimeout(function(){
						
						//swal("Ajax request finished!");
						document.frm1.action="final_replacement_updated.php";
						document.frm1.submit();
						
					  }, 2000);
					});
				
			}

				//document.frm1.action="final_replacement.php";
				//document.frm1.submit();
				
				
			  
		});
});
	
</script>
<!--Drage_EN-->