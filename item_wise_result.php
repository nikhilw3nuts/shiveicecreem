<?php
	$filename = "Item wise result";
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");
	
	$startdate = date('d-m-Y');
	$enddate = date('d-m-Y');
	
?>
	<div class="content-wrapper">
		<section class="content-header">
        	<h1>
            	<?=$filename?>
            	<small>it all starts here</small>
            </h1>
         	<ol class="breadcrumb">
            	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            	<li class="active"><?=$filename?></li>
          	</ol>
        </section>
        <section class="content">
        	<div class="box box-primary">
                <div class="box-header with-border">
	            	<h3 class="box-title">Filter by date</h3>
	            	<div class="box-tools pull-right"></div>
            	</div>
                <div class="row">
                	<div class="col-md-6">
                        <form role="form" class="form-horizontal" name="frm_additem" id="frm_additem" method="post" enctype="multipart/form-data" >
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">From Date</label>
                                    <div class="col-sm-9">
                                        <input id="datepicker1" value="<?php echo $startdate ?>" placeholder="From date" name="fromdate" class="form-control pull-right" type="text" required="">
                                    </div>
                                </div>	
                                <div class="form-group">	
                                    <label class="col-sm-3 control-label" for="inputEmail3">To Date</label>
                                    <div class="col-sm-9">
                                        <input id="datepicker" value="<?php echo $enddate ?>" class="form-control pull-right" type="text" placeholder="To Date" name="todate" required="">
                                    </div>
                                </div>  
                            </div>  
                            
                            <div class="box-footer col-md-9" align="center">
                                <button class="btn btn-primary" type="submit">Go</button>
                            </div>
                        </form>              
                    </div> 
                </div>
                <hr style="border-width: 5px;">
                <div class="row">
                    <div class="col-md-8 center-block float-none">
                        <div class="portlet light bordered">
                            <div class="portlet-title"><div class="caption font-dark"><span class="caption-subject bold uppercase">&nbsp;</span></div></div>
                            <div class="portlet-body">
                                <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                                    <div class="table-scrollable">
                                        <table border="1" id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" role="grid" aria-describedby="sample_1_info">
                                            <thead>
                                                <tr role="row" align="center" class="text_center">
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Item id </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Item Name </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Quantity </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Weight </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Price (per Item) </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Total Billed Amount </th>
                                                </tr>
                                                <?php 
                                                    $selqryweight = "SELECT weight FROM `bill_item_master` group by weight";
                                                    $selectWeightRes = mysql_query($selqryweight) or die(mysql_error());
                                                    $total_qty = 0;	
                                                    $total_amt = 0;
                                                    while ($selectWeightRow = mysql_fetch_array($selectWeightRes)) {
                                                        if ($selectWeightRow['weight'] == '0') {
                                                            $joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
                                                            $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
                                                            while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
                                                                ?>
                                                                    <tr class="gradeX odd" role="row">
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
                                                                        <td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
                                                                        <td align="right"><?php echo $selectWeightRow['weight']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
                                                                    </tr>
                                                                <?php
                                                                $total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
                                                                $total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
                                                            }
                                                        }
                                                        elseif($selectWeightRow['weight'] == '250')
                                                        {
                                                            $joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
                                                            $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
                                                            while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
                                                                    
                                                                ?>
                                                                    <tr class="gradeX odd" role="row">
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
                                                                        <td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
                                                                        <td align="right">250 Gm</td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
                                                                    </tr>
                                                                <?php
                                                                $total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
                                                                $total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
                                                            }	
                                                        }
                                                        elseif($selectWeightRow['weight'] == '500')
                                                        {
                                                            $joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
                                                            $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
                                                            while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
                                                                    
                                                                ?>
                                                                    <tr class="gradeX odd" role="row">
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
                                                                        <td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
                                                                        <td align="right">500 Gm</td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
                                                                    </tr>
                                                                <?php
                                                                $total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
                                                                $total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
                                                            }
                                                        }
                                                        elseif($selectWeightRow['weight'] == '750')
                                                        {
                                                            $joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
                                                            $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
                                                            while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
                                                                    
                                                                ?>
                                                                    <tr class="gradeX odd" role="row">
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
                                                                        <td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
                                                                        <td align="right">750 Gm</td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
                                                                    </tr>
                                                                <?php
                                                                $total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
                                                                $total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
                                                            }
                                                        }
                                                        elseif($selectWeightRow['weight'] == '1000')
                                                        {
                                                            $joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
                                                            $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
                                                            while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
                                                                    
                                                                ?>
                                                                    <tr class="gradeX odd" role="row">
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
                                                                        <td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
                                                                        <td align="right">1 Kg</td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
                                                                    </tr>
                                                                <?php
                                                                $total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
                                                                $total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
                                                            }
                                                        }
                                                    }
                                                ?>
                                                <tr class="gradeX odd" role="row">
                                                    <td align="right"></td>
                                                    <td>Total</td>
                                                    <td align="right"><?php echo $total_qty; ?></td>
                                                    <td align="right"></td>
                                                    <td align="right"></td>
                                                    <td align="right"><?php echo $total_amt; ?></td>
                                                </tr>     
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
        	</div>
        </section>
    </div>      
<script>
	jQuery(document).ready(function() {
		
		$('#datepicker').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy',
			todayHighlight: true
		});
				
		$('#datepicker1').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy',
			todayHighlight: true
		});		
		
        jQuery("#frm_additem").submit(function(e){
			e.preventDefault();
			var cpval = jQuery( this ).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo 'ajaxitem_wise_list.php' ?>",
				data: cpval,
				cache: false,
					success: function(html) {
						//alert(html);
						jQuery("#sample_1").html(html);
						
					}	
			});
			return false;  
		})
		
		
    });
</script>
<?php
	include_once("includes/footer.php");
?>
      
