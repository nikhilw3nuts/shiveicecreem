<?php 
	include_once("includes/connection_main.php");
	if(isset($_SESSION["siteadmin"]) && $_SESSION["siteadmin"]=="I am The Admin Logged In")
	{
		if($_SESSION['user_type'] == 2)
		{			
			header ("Location:".$ROOT_SITE_URL."menu/index.php");
		}	
		else
		{		
			header ("Location:dashbord.php");
			exit;
		}
	}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=$ROOT_SITE_URL?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=$ROOT_SITE_URL?>dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=$ROOT_SITE_URL?>plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
  

<div id="bg">
  <img src="images/bg_wall_v.jpg" alt="">
</div>
<style type="text/css">
#bg {
  position: fixed; 
  top: -50%; 
  left: -50%; 
  width: 200%; 
  height: 200%;
  z-index:-100;
}
#bg img {
  position: absolute; 
  top: 0; 
  left: 0; 
  right: 0; 
  bottom: 0; 
  margin: auto; 
  min-width: 50%;
  min-height: 50%;
  opacity: 0.3;
    filter: alpha(opacity=40); /* For IE8 and earlier */
}
.error_msg {
	background: #b84c4c;
	border: 1px solid #712d2d;
	padding:0px 10px;
	margin-bottom:10px;
	color: white;
	text-shadow: none;
}
</style>

  
    <div class="login-box">
      <div class="login-logo">
        <a href="#"><strong>Admin</strong></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
		 <div id="response" class="error_msg"></div>
       		<form class="login_validate" method="post" accept-charset="utf-8">
          <div class="form-group has-feedback">
            <input  class="form-control"  type="text" name="username" id="username" placeholder="Please enter username" required="required">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input  class="form-control" type="password" name="password" id="password" placeholder="Please enter password" required="required">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=$ROOT_SITE_URL?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=$ROOT_SITE_URL?>bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?=$ROOT_SITE_URL?>plugins/iCheck/icheck.min.js"></script>
    
	<script type="text/javascript">
	$("document").ready(function(){
		$(".login_validate").submit(function(){
			var data = {
				"action": "test"
			};
		
			data_send = $(this).serialize() + "&" + $.param(data);
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: 'login_validate_db.php',
				data: $(this).serialize()
				//data: data_send.serialize()
			
			})
			.done(function(data){
				 
				 if(data['status'] == "fail"){
					 
					 console.log(data['status']);
					 $('#response').html("Please enter valid username and password.");
					 
				 }else if(data['status'] == "success"){
		
					  location.href='dashbord.php';
				 }
				// show the response
				//$('#response').html(data);
				 
			})
			
			return false;
		});
	});
</script>	
	
  </body>
</html>
