<?php
	$filename = "My Account";
	
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	
	$qry = "SELECT * FROM siteadmin  ";
	$result = mysql_query($qry) or die ("Error selecting from admin".mysql_error());
	$row = mysql_num_rows($result);
	$records = mysql_fetch_assoc($result);
	
	if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'success'){
		$msg = "Details updated successfuly!";	
		$msg_class = "msg_success";
	}


?>
  

      <!-- Content Wrapper. Contains page content -->
    
	
	  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $filename;?>
            <small>Manage your profile details </small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $filename;?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
           
		   <!--MSG_ST-->
				<?php
				if(isset($_REQUEST['err']) && $_REQUEST['err'] == "del"){
					$msg = "Details has been deleted";
				}else if(isset($_REQUEST['err']) && $_REQUEST['err'] == "ins"){
					$msg = "Details has been added";
				}else if(isset($_REQUEST['err']) && $_REQUEST['err'] == "none"){
					$msg = "Details updated successfully";
				}else if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == "success"){
					$msg = "Details updated successfully";
				}
				?>
				
				<?php 
				if( $msg != ""  && ( $_REQUEST['err'] == "none"  ||  $_REQUEST['msg'] == "success" ) ){ 
				?> 
				<div class="alert alert-success alert-dismissible">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
					<h4><i class="icon fa fa-check"></i> <?php echo $msg ; ?>!</h4>
					
				  </div>
				<?php
				} ?>
				
		   
		   
		   <!--Main_Content_ST-->
		   
		   <script type="text/javascript">
		$("document").ready(function(){
			
			$("#frm_myaccount").submit(function(e){
				e.preventDefault();
			
				var data = {
					"action": "test"
				};
			
				data_send = $(this).serialize() + "&" + $.param(data);
				$.ajax({
					dataType: 'json',
					type: 'POST',
					url: 'settings_db.php',
					data: $(this).serialize()
					//data: data_send.serialize()
				
				})
				.done(function(data){
					 
					 if(data['status'] == "fail"){
						 
						 console.log(data['status']);
						 $('#response').addClass('msg_failed');				 
						  $('#response').html(data['message']);				
						
					 }else if(data['status'] == "success"){
						//$('#response').addClass('msg_success');
						//$('#response').html("failed");
						 // location.href='dashbord.php';
						 //location.reload();
						 window.location.href = window.location.href + "?msg=success";
						 
					 }
					 
				})
				
				return false;
			});
		});
</script>
<style>

.msg_failed {
	background: #b84c4c;
	border: 1px solid #712d2d;
	padding:0px 10px;
	margin-bottom:10px;
	color: white;
	text-shadow: none;
	display:block;
}
</style>
		   <div class="col-md-9">
              <div class="nav-tabs-custom">
                
                <div class="tab-content">
                  <div id="settings" class="tab-pane active">
        			<div id="response"></div>
					<form id="frm_myaccount" name="frm_myaccount"  method="post" class="form-horizontal">
	  
					<div id="myTabContent" class="tab-content">
					  <div class="tab-pane active in" id="home">
					  
					  <div class="form-group">
							<label class="col-sm-2 control-label">Full Name</label>
							<div class="col-sm-10">
								<input  required type="text" id="fullname" name="fullname" value="<?php echo $records["fullname"];?>" class="form-control">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Email</label>
							<div class="col-sm-10">	
								<input type="email" required  id="adminemail" name="adminemail" value="<?php echo $records["adminemail"];?>" class="form-control">
							</div>	
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Username</label>
							<div class="col-sm-10">
								<input required type="text" id="username" name="username" value="<?php echo $records["username"];?>" class="form-control">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Password</label>
							<div class="col-sm-10">
								<input required type="password" id="password" name="password" value="" class="form-control">
							</div>
						</div>
						
					   <div class="form-group">
						<label class="col-sm-2 control-label">New Password</label>
							<div class="col-sm-10">
								<input type="password" id="new_password" name="new_password" value="" class="form-control">
							</div>	
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">Verify Password</label>
							<div class="col-sm-10">
								<input type="password" id="new_password_again" name="new_password_again" value="" class="form-control">
							</div>
						</div>
					
					
					  </div>
					</div>
					<div align="center">
					 <br/>
					  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					  &nbsp;
					  <button type="button" class="btn btn-primary" onclick="location.href='dashbord.php'">Cancle</button>
					</div>
					
	    			</form>
		
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div>
		   <!--Main_Content_EN-->
		   
            </div><!-- /.box-body -->
            <div class="box-footer">
            <!--  Footer-->
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div>
	
	  <!-- /.content-wrapper -->
	  
	  


 <?php
	include_once("includes/footer.php");
?>
      
