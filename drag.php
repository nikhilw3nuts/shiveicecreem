<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js" ></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

</head>

<body>

<div id="table1" class="bitacoratable">
    <table >
	                <tr class="childrow">
                    <td collspan = "1" >
                        <table class="childgrid">
                                <tr class="draggable_tr">
                                    <td>1</td>
                                    <td>Student 1</td>                                             
                                </tr>
                                <tr class="draggable_tr">
                                    <td>2</td>
                                    <td>Student 2</td>                                                                              
                                </tr>       
                                <tr class="draggable_tr">
                                    <td>3</td>
                                    <td>Student 3</td>                                                                              
                                </tr>  
                                <tr class="draggable_tr">
                                    <td>4</td>
                                    <td>Student 4</td>                                                                              
                                </tr>  
                                <tr class="draggable_tr">
                                    <td>5</td>
                                    <td>Student 5</td>                                                                              
                                </tr>                              
                        </table>
                    </td>
                </tr>
	</table>
</div>

<!--HTML_ST-->
	<div id="table2" class="bitacoratable">
	 <table >
	                <tr class="childrow">
                    <td collspan = "1" >
		               <table class="childgrid">
                                <tr class="draggable_tr">
                                    <td>1</td>
                                    <td>Student 1</td>                                             
                                </tr>
                                <tr class="draggable_tr">
                                    <td>2</td>
                                    <td>Student 2</td>                                                                              
                                </tr>       
                                <tr class="draggable_tr">
                                    <td>3</td>
                                    <td>Student 3</td>                                                                              
                                </tr>  
                                <tr class="draggable_tr">
                                    <td>4</td>
                                    <td>Student 4</td>                                                                              
                                </tr>  
                                <tr class="draggable_tr">
                                    <td>5</td>
                                    <td>Student 5</td>                                                                              
                                </tr>                              
                        </table>
					</td>
					</tr>
					</table>
	</div>
<!--HTML_EN-->


<style type="text/css">
table
{
border-collapse:collapse;
}
table, td, th
{
border:1px solid black;
}
        .bitacoratable {
            height: 400px;
            overflow-y: auto;
            width: 220px;
            float:left;
        }
        #table1 {
            margin-right: 100px;
        }
        .childgrid tr { cursor: pointer; }
        .selectedRow, .selectedRow:active, .childgrid tr:active {
            background-color: #E7E7E7;
            cursor: move;
        }
</style>

<script type="text/javascript">
$("#table1 .childgrid tr, #table2 .childgrid tr").draggable({
      helper: function(){
          var selected = $('.childgrid tr.selectedRow');
        if (selected.length === 0) {
          selected = $(this).addClass('selectedRow');
        }
        var container = $('<div/>').attr('id', 'draggingContainer');
    container.append(selected.clone().removeClass("selectedRow"));
    return container;
      }
 });

$("#table1 .childgrid, #table2 .childgrid").droppable({
    drop: function (event, ui) {
    $(this).append(ui.helper.children());
    $('.selectedRow').remove();
    }
});

$(document).on("click", ".childgrid tr", function () {
    $(this).toggleClass("selectedRow");
});

$( "#draggable" ).draggable();

</script>
</body>
</html>
