<?php

function setGPC($val,$act)
{
	if(!get_magic_quotes_gpc())
		$val = addslashes(trim($val));
	
	if($act == "display")
		$val = stripslashes($val);
	
	//$val = preg_replace("/<!--[\s\S]*?-->/", "",$val);
	
	return $val;		
}


function setDB($val,$act)
{
	# use this function to display/insert values coming from sources other than Get/Post/Cookie. i.e. database/file etc.
	# parameter "act" should have a value "display" if it is being used for displaying value. 
	# in case of database update/insert the "act" can be left blank.

	if(!get_magic_quotes_runtime())
		$val = addslashes(trim($val));
	
	if($act == "display")
		$val = stripslashes($val);
		
	$val = preg_replace("/<!--[\s\S]*?-->/", "",$val);

	return $val;		
}

function populateFields($src,$txtFlds,$numFlds,$myRow)
{
	$myFlds = Array();
	
	if($src=="Initialize")
	{
		for($b = 0;$b< count($txtFlds)+count($numFlds);$b++)
		{
			if($b<count($txtFlds))
				$myFlds[$txtFlds[$b]] = "";
			else
				$myFlds[$numFlds[$b-count($txtFlds)]] = "";
		}	
	}
	elseif($src=="GPC")
	{
		for($b = 0;$b< count($txtFlds)+count($numFlds);$b++)
		{
			if($b<count($txtFlds))
			{
				$myVal = isset($_POST[$txtFlds[$b]])?trim($_POST[$txtFlds[$b]]):"";
				$myFlds[$txtFlds[$b]] = (strlen($myVal)==0)?"":setGPC($myVal,"display");
			}
			else
			{
				$myVal = isset($_POST[$numFlds[$b-count($txtFlds)]])?trim($_POST[$numFlds[$b-count($txtFlds)]]):"";
				$myFlds[$numFlds[$b-count($txtFlds)]] = (intval($myVal)==0)?"":setGPC($myVal,"display");	
			}
		}	
	}
	elseif($src=="DB")
	{
		for($b = 0;$b<count($txtFlds)+count($numFlds);$b++)
		{
			if($b<count($txtFlds))
			{
				$myFlds[$txtFlds[$b]] = ((!isset($myRow[$txtFlds[$b]])) || strlen(trim($myRow[$txtFlds[$b]]))==0)?"":setDB($myRow[$txtFlds[$b]],"display");
			}
			else
				$myFlds[$numFlds[$b-count($txtFlds)]] = ((!isset($myRow[$numFlds[$b-count($txtFlds)]])) || 
																					intval(trim($myRow[$numFlds[$b-count($txtFlds)]]))==0)?"0":
																					setDB($myRow[$numFlds[$b-count($txtFlds)]],"display");	
		}	
	}
	
	return $myFlds;	
}

Function UpdateFlds($myTxtFlds,$myNumFlds)
{

	if(!isset($myTxtFlds) || !is_array($myTxtFlds))
		$myTxtFlds = Array();
		
	if(!isset($myNumFlds) || !is_array($myNumFlds))
		$myNumFlds = Array();

	$qryUpdate = Array();

	for($b = 0;$b < count($myTxtFlds)+count($myNumFlds);$b++)
	{
		if ($b<count($myTxtFlds))
		{
			$myVal=isset($_POST[$myTxtFlds[$b]])?trim($_POST[$myTxtFlds[$b]]):"";
			$qryUpdate[$b] = $myTxtFlds[$b]."=";
			$qryUpdate[$b] .= (strlen ($myVal) == 0)?"''":("'".setGPC ($myVal,"")."'");
		}
		else
		{
			$myVal = isset($_POST[$myNumFlds[$b-count($myTxtFlds)]])?$_POST[$myNumFlds[$b-count($myTxtFlds)]]:0;
			$qryUpdate[$b] = $myNumFlds[($b-count($myTxtFlds))]." = ";
			$qryUpdate[$b] .= (intval($myVal) == 0)?"0":setGPC ($myVal,"");
		}
					
	}
	
	$qryUpdate = implode(",",$qryUpdate);

	return $qryUpdate;
}

function myForm($action, $err, $errfld)
{
	global $_POST;
	
	echo "<br>";
	echo "<form name=\"frmpostback\" method=\"post\" action=\"$action\">\n";
	echo "<input type=\"hidden\" name=\"err\" value=\"$err\">\n";
	echo "<input type=\"hidden\" name=\"errfld\" value=\"$errfld\">\n";
		
	foreach($_POST as $key=>$value)
	{
		if(is_array($value))
			$myValue = setGPC(implode(",",$value),"display");
		else
			$myValue = setGPC($value,"display");

		print "<input type=\"hidden\" name=\"$key\" value=\"$myValue\">\n";
	}

	echo "</form>
		<script language='Javascript'>
			document.frmpostback.submit();
		</script>";
	exit;
}

function checkCompulsory($CompulsoryTxtFlds,$CompulsoryNumFlds,$FriendlyNames,$redirection)
{
	if(!isset($CompulsoryTxtFlds) || !is_array($CompulsoryTxtFlds))
		$CompulsoryTxtFlds = Array();
		
	if(!isset($CompulsoryNumFlds) || !is_array($CompulsoryNumFlds))
		$CompulsoryNumFlds = Array();

	for($c=0;$c<count($CompulsoryTxtFlds)+count($CompulsoryNumFlds);$c++)
	{
		if($c<count($CompulsoryTxtFlds))
		{
			if(!isset($_POST[$CompulsoryTxtFlds[$c]]) || strlen(trim($_POST[$CompulsoryTxtFlds[$c]])) == 0)
			{
				if(isset($FriendlyNames[$c]))
					$errfld = $FriendlyNames[$c];
				else
					$errfld = $CompulsoryTxtFlds[$c];
					
				myForm($redirection,"compulsory",$errfld);
				exit;
			}
		}
		else
		{
			if(!isset($_POST[$CompulsoryNumFlds[$c-count($CompulsoryTxtFlds)]]) || intval($_POST[$CompulsoryNumFlds[$c-count($CompulsoryTxtFlds)]]) == 0)
			{
				if(isset($FriendlyNames[$c]))
					$errfld = $FriendlyNames[$c];
				else
					$errfld = $CompulsoryTxtFlds[$c-count($CompulsoryTxtFlds)];

				myForm($redirection,"compulsory",$errfld);
				exit;
			}
		}
	}
}

function validateEmail($val)
{
	$my=$val;
	$attherate= strpos($my,"@");
	$lastattherate = strrpos($my,"@");
	$dotpos= strrpos($my,".");
	$posspace = strpos($my," ");
	$totallen = strlen($my);
	
	if ($attherate<=0 || $dotpos<=0 || $attherate > $dotpos || ($dotpos-$attherate)<=1 || ($dotpos == $totallen-1) || $posspace > -1 || $attherate!=$lastattherate)
	{
		return false;
	}
	else
		return true;
}

function escape_string($estr)
{
	if(version_compare(phpversion(),'4.3.0')=='-1') return(mysql_escape_string(trim($estr))); else return(mysql_real_escape_string(trim($estr)));
}

function formatAllSpecialCharWithJson($val,$act)
{
	
	if($act == "input")
	{
		$val = htmlentities($val);
	}
	else
	{
		$val = json_encode(utf8_encode(html_entity_decode($val)));
	}	
	
	return $val;
}

function pagination_1() {
    global $page, $Keywords_Search;
    
    echo '<form name="frmPagination" method="POST" action="">';
    echo '<input type="hidden" name="page" value="'.$page.'" />';
    echo '<input type="hidden" name="keywords_search" value="'.$Keywords_Search.'" />';
    echo '</form>';
}

 function pagination($total,$per_page=10,$page=1,$url='?'){   
    $adjacents = "2"; 
     
    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";
	$lastlabel = "Last &rsaquo;&rsaquo;";
     
    $page = ($page == 0 ? 1 : $page);  
    $start = ($page - 1) * $per_page;                               
     
    $prev = $page - 1;                          
    $next = $page + 1;
     
    $lastpage = ceil($total/$per_page);
     
    $lpm1 = $lastpage - 1; // //last page minus 1
     
    $pagination = "";
    if($lastpage > 1){   
        $pagination .= "<ul class='pagination'>";
        
             
            if ($page > 1) $pagination.= "<li><a href='{$url}page={$prev}'>{$prevlabel}</a></li>";
             
        if ($lastpage < 7 + ($adjacents * 2)){   
            for ($counter = 1; $counter <= $lastpage; $counter++){
                if ($counter == $page)
                    $pagination.= "<li><a class='current'>{$counter}</a></li>";
                else
                    $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
            }
         
        } elseif($lastpage > 5 + ($adjacents * 2)){
             
            if($page < 1 + ($adjacents * 2)) {
                 
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
                }
                //$pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";  
                     
            } elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                 
                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                //$pagination.= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
                }
                //$pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";      
                 
            } else {
                 
                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                    
                }
            }
        }
         
            if ($page < $counter - 1) {
				$pagination.= "<li><a href='{$url}page={$next}'>{$nextlabel}</a></li>";
				$pagination.= "<li><a href='{$url}page=$lastpage'>{$lastlabel}</a></li>";
			}
         
        $pagination.= "</ul>";        
        $pagination .= "<div class='page_info'>Page {$page} of {$lastpage}</div>";
    }
     
    return $pagination;
}

function encode5t($str) {
    for ($i = 0; $i < 5; $i++) {
        $str = strrev(base64_encode($str)); //apply base64 first and then reverse the string
    }
    return $str;
}

function decode5t($str) {
    for ($i = 0; $i < 5; $i++) {
        $str = base64_decode(strrev($str)); //apply base64 first and then reverse the string}
    }
    return $str;
}

function send_mail()
{
}


function replaceit($str){

	$str_array  = array_map('intval', str_split($str));
	
	$one = $str_array[0];
	$five = $str_array[4];	

	$str_array[0] = $five;
	$str_array[4] = $one;	

	$str = implode("",$str_array);

	return $str;
}

function enc($str,$key) {

	// switching digits 
	$str = replaceit($str);
	// switching digits 
	
		
    for ($i = 0; $i < 6; $i++) {
        $str = strrev(base64_encode($str)); //apply base64 first and then reverse the string
    }

	$key = base64_encode($key);	
	$str = $key.$str;
	
    return $str;
}

function dec($str,$key) {

	// removing key
	$key = base64_encode($key);		
	$key1 = $key;
	$str = str_replace($key1,"",$str);
	// removing key
	
    for ($i = 0; $i < 6; $i++) {
        $str = base64_decode(strrev($str)); //apply base64 first and then reverse the string}
    }

	// switching digits 
	$str = replaceit($str);
	// switching digits 
	
    return $str;
}

// Get Location Address FROM Latitude - Longitude : START
function Get_Address_From_Google_Maps($lat, $lon) {

$url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&sensor=false";

// Make the HTTP request
$data = @file_get_contents($url);
// Parse the json response
$jsondata = json_decode($data,true);

// If the json data is invalid, return empty array
if (!check_status($jsondata))   return array();

$address = array(
    'country' => google_getCountry($jsondata),
    'province' => google_getProvince($jsondata),
    'city' => google_getCity($jsondata),
    'street' => google_getStreet($jsondata),
    'postal_code' => google_getPostalCode($jsondata),
    'country_code' => google_getCountryCode($jsondata),
    'formatted_address' => google_getAddress($jsondata),
);

return $address;
}

/* 
* Check if the json data from Google Geo is valid 
*/

function check_status($jsondata) {
    if ($jsondata["status"] == "OK") return true;
    return false;
}

/*
* Given Google Geocode json, return the value in the specified element of the array
*/

function google_getCountry($jsondata) {
    return Find_Long_Name_Given_Type("country", $jsondata["results"][0]["address_components"]);
}
function google_getProvince($jsondata) {
    return Find_Long_Name_Given_Type("administrative_area_level_1", $jsondata["results"][0]["address_components"], true);
}
function google_getCity($jsondata) {
    return Find_Long_Name_Given_Type("locality", $jsondata["results"][0]["address_components"]);
}
function google_getStreet($jsondata) {
    return Find_Long_Name_Given_Type("street_number", $jsondata["results"][0]["address_components"]) . ' ' . Find_Long_Name_Given_Type("route", $jsondata["results"][0]["address_components"]);
}
function google_getPostalCode($jsondata) {
    return Find_Long_Name_Given_Type("postal_code", $jsondata["results"][0]["address_components"]);
}
function google_getCountryCode($jsondata) {
    return Find_Long_Name_Given_Type("country", $jsondata["results"][0]["address_components"], true);
}
function google_getAddress($jsondata) {
    return $jsondata["results"][0]["formatted_address"];
}

/*
* Searching in Google Geo json, return the long name given the type. 
* (If short_name is true, return short name)
*/

function Find_Long_Name_Given_Type($type, $array, $short_name = false) {
    foreach( $array as $value) {
        if (in_array($type, $value["types"])) {
            if ($short_name)    
                return $value["short_name"];
            return $value["long_name"];
        }
    }
}

/*
*  Print an array
*/

function d($a) {
    echo "<pre>";
    print_r($a);
    echo "</pre>";
}
// Get Location Address FROM Latitude - Longitude : END

function get_cleaners() {
	
	$myReturn = array();
	
	$query = "SELECT cleaner_id, cleaner_code, cleaner_first_name, cleaner_last_name FROM cleaner_master ORDER BY cleaner_first_name, cleaner_last_name";
	$result = mysql_query($query) or print('<option>No Cleaner Selection</option>');
	while($arow = mysql_fetch_assoc($result)) {
		$myReturn[$arow["cleaner_id"]] .= $arow["cleaner_first_name"]." ".$arow["cleaner_last_name"];
	}
	
	return $myReturn;
}

function get_items($sel_id = ""){

		$items = "";
		$sel_item_qry = "SELECT * from  item_master order by  itemname";
		$sel_item_res = mysql_query($sel_item_qry);
		$sel_item_num_rows = mysql_num_rows($sel_item_res);
		if($sel_item_num_rows > 0){
			echo '<select class="form-control" name="bill_items[]" id="bill_status" required >
						<option value="">Select Item</option>';
			while($items = mysql_fetch_assoc($sel_item_res)){
				$sel = "";
				if($sel_id != "" && $sel_id == $items['itemid']){ $sel = 'selected="selected"'; }
				echo  '<option data-price="'.$items['itemprice'].'" '.$sel.' value="'.$items['itemid'].'" >'.$items['itemname'].' ( Rs. '.$items['itemprice'].') </option>';
			}	
			echo '</select>';	
		}
}

function get_items_price($itemid){
		$item_price = "";
		$sel_item_qry = "SELECT itemprice from  item_master WHERE itemid = '".$itemid."' ";
		$sel_item_res = mysql_query($sel_item_qry);
		$sel_item_num_rows = mysql_num_rows($sel_item_res);
		if($sel_item_num_rows > 0){
			$items = mysql_fetch_assoc($sel_item_res);
			$item_price = $items['itemprice'];
		}

	return $item_price;
}

function get_bill_total($billid){
		$item_price = "";
		
		$sel_item_qry = "SELECT sum(bim_item_price) as bill_total from  bill_item_master WHERE billid = '".$billid."' ";
		$sel_item_res = mysql_query($sel_item_qry);
		$sel_item_num_rows = mysql_num_rows($sel_item_res);
		if($sel_item_num_rows > 0){
			$items = mysql_fetch_assoc($sel_item_res);
			$item_price = $items['bill_total'];
		}

		return $item_price;
}


function get_temp_bill_total($billid){
		$item_price = "";
		
		$sel_item_qry = "SELECT sum(bim_item_price) as bill_total from temp_bill_item_master WHERE billid = '".$billid."' ";
		$sel_item_res = mysql_query($sel_item_qry);
		$sel_item_num_rows = mysql_num_rows($sel_item_res);
		if($sel_item_num_rows > 0){
			$items = mysql_fetch_assoc($sel_item_res);
			$item_price = $items['bill_total'];
		}

		return $item_price;
}


function get_original_bill_total($billid){
		$item_price = "";
		
		$sel_item_qry = "SELECT sum(bim_item_price) as bill_total from  bill_item_master_original WHERE billid = '".$billid."' ";
		$sel_item_res = mysql_query($sel_item_qry);
		$sel_item_num_rows = mysql_num_rows($sel_item_res);
		if($sel_item_num_rows > 0){
			$items = mysql_fetch_assoc($sel_item_res);
			$item_price = $items['bill_total'];
		}

		return $item_price;
}


function total_bills($total_bills){
		 
		 //print_r($total_bills);
		 
		$item_price = "";
		 $billid_list = implode(',', array_values($total_bills));
		 
		$sel_item_qry = "SELECT sum(bim_item_price) as bill_total from  bill_item_master WHERE billid  IN  (".$billid_list.") ";
		$sel_item_res = mysql_query($sel_item_qry);
		$sel_item_num_rows = mysql_num_rows($sel_item_res);
		if($sel_item_num_rows > 0){
			$items = mysql_fetch_assoc($sel_item_res);
			$item_price = $items['bill_total'];
		}

		return $item_price;
		
}

function get_bill_item_quantity($bill_id){
	
	$item_qnt = "0";
	$item_qnt_sum = "0";
	$final_qnt = "";
	
	$sel_item_qnt_qry = "SELECT COUNT(*) AS item_cnt, sum(bim_item_quantity) as item_sum FROM bill_item_master WHERE billid = '".$bill_id."' ";
	$sel_item_qnt_res = mysql_query($sel_item_qnt_qry);
	$sel_item_qnt_row = mysql_num_rows($sel_item_qnt_res);
	
	if($sel_item_qnt_row > 0){
		$sel_item_qnt_rec = mysql_fetch_assoc($sel_item_qnt_res);
		$item_qnt = $sel_item_qnt_rec['item_cnt'];
		$item_qnt_sum = $sel_item_qnt_rec['item_sum'];
				
		$final_qnt = $item_qnt." (".$item_qnt_sum." qnt)";
	}
	
	//return $item_qnt;
	return $final_qnt;
}

function get_bill_item_quantity_new($bill_id){
	
	$item_qnt = array();
	
	$sel_item_qnt_qry = "SELECT COUNT(*) AS item_cnt, sum(bim_item_quantity) as item_sum FROM bill_item_master WHERE billid = '".$bill_id."' ";
	$sel_item_qnt_res = mysql_query($sel_item_qnt_qry);
	$sel_item_qnt_row = mysql_num_rows($sel_item_qnt_res);
	
	if($sel_item_qnt_row > 0){
		$sel_item_qnt_rec = mysql_fetch_assoc($sel_item_qnt_res);
		
		$item_qnt["item_tot"] = $sel_item_qnt_rec['item_cnt'];
		$item_qnt["item_qnt_tot"] = $sel_item_qnt_rec['item_sum'];
				
	}

	return $item_qnt;
}

function total_temp_bills($total_bills){
		 
		 //print_r($total_bills);
		 
		$item_price = "";
		 $billid_list = implode(',', array_values($total_bills));
		 
		$sel_item_qry = "SELECT sum(bim_item_price) as bill_total from  temp_bill_item_master WHERE billid  IN  (".$billid_list.") ";
		$sel_item_res = mysql_query($sel_item_qry);
		$sel_item_num_rows = mysql_num_rows($sel_item_res);
		if($sel_item_num_rows > 0){
			$items = mysql_fetch_assoc($sel_item_res);
			$item_price = $items['bill_total'];
		}

		return $item_price;
		
}

function total_original_bills($total_bills){
		 
		 //print_r($total_bills);
		 
		$item_price = "";
		 $billid_list = implode(',', array_values($total_bills));
		 
		$sel_item_qry = "SELECT sum(bim_item_price) as bill_total from  bill_item_master_original WHERE billid  IN  (".$billid_list.") ";
		$sel_item_res = mysql_query($sel_item_qry);
		$sel_item_num_rows = mysql_num_rows($sel_item_res);
		if($sel_item_num_rows > 0){
			$items = mysql_fetch_assoc($sel_item_res);
			$item_price = $items['bill_total'];
		}

		return $item_price;
		
}


function printit(){
	
	echo "<hr>";
	echo "SESSION";	
	echo "<hr>";
	echo "<pre>";	
	print_r($_SERVER);
	echo "</pre>";		
	
	echo "<hr>";
	echo "REQUEST";	
	echo "<hr>";
	echo "<pre>";	
	print_r($_REQUEST);
	echo "</pre>";		
	
	echo "<hr>";
	echo "POST";	
	echo "<hr>";
	echo "<pre>";	
	print_r($_POST);
	echo "</pre>";		

	echo "<hr>";
	echo "FILES";	
	echo "<hr>";
	echo "<pre>";	
	print_r($_FILES);
	echo "</pre>";		

}


function make_deduction_01($deduction_amount,$original_amount){

	$condition = " billid IN ( SELECT billid FROM bill_master WHERE bill_status = 'nottaken' )  ";
	
	if( $deduction_amount  >  0){
					$sel_max_bill_qry = "SELECT * FROM bill_item_master WHERE $condition  AND  bim_item_quantity > 1 ORDER BY bim_item_quantity_price DESC ";
					$sel_max_bill_res = mysql_query($sel_max_bill_qry);
					$sel_max_bill_row = mysql_num_rows($sel_max_bill_res);
					if($sel_max_bill_row > 0)
					{
							
							while( $row = mysql_fetch_assoc($sel_max_bill_res)){
								
									/*
										echo '<hr/>';
										echo $row['bim_id'];
										echo "   ===   ";
										echo $row['bim_item_price'];
										echo '<hr/>';
									*/
										// 1250
										if($deduction_amount  > $row['bim_item_quantity_price']    &&   $deduction_amount  > 0 ){
												
												$new_qnt = $row['bim_item_quantity'] - 1;		// Deduct total number of quantity 
												$new_bim_item_price = $row['bim_item_price'] - $row['bim_item_quantity_price'] ;		// Deduct total number of final price 
												$deduction_amount = $deduction_amount -  $row['bim_item_quantity_price'];		// Reduce Final loop
												
												//echo '<br/><br/>';
												echo '<br/>';
												echo '***aaa*** '.$deduction_amount.'***aaa***';
												echo '<br/>';
												
												echo $update_query = "UPDATE bill_item_master SET 
													bim_item_quantity = '".$new_qnt ."' ,
													bim_item_price = '".$new_bim_item_price."'
													WHERE bim_id = '".$row['bim_id']."'
													";
												$update_query_res = mysql_query($update_query);

												echo '<hr>';
													
										}else{
										
												//echo $sel_max_bill_qry; exit;
												
												return $deduction_amount;
												exit;
										
										}
							}
							
							
							// Recursive Function Call ST -----------------------
							if($deduction_amount > 0){
									make_deduction($deduction_amount,$original_amount);
							}
							// Recursive Function Call EN -----------------------
						
					}else{
		
										$condition = " billid IN ( SELECT billid FROM bill_master WHERE bill_status = 'nottaken' )  ";
										
										// WHEN only one quantity left 
										$sel_top_bill_qry = 
										'SELECT bim_id, billid, 
										count(billid) as itmcnt, 
										sum(bim_item_price) as billtotal 
											FROM bill_item_master 
										WHERE '. $condition.' 
										GROUP by billid
										HAVING count(billid) > 1
										ORDER by billtotal DESC';
			
										$sel_top_bill_res = mysql_query($sel_top_bill_qry);
										$sel_top_bill_row = mysql_num_rows($sel_top_bill_res);
			
										if($sel_top_bill_row > 0   &&   $deduction_amount  > 0 ){
													
													 echo '<br/>';
													 echo '###BBB### ';
													 echo $deduction_amount;
													 echo '<br/>';
													 
													while( $top_row = mysql_fetch_assoc($sel_top_bill_res)){
															if($top_row['itmcnt']  > 1 ){
															
																			$sel_rec_qry = "SELECT * FROM bill_item_master WHERE billid = '".$top_row['billid']."' 
																			ORDER BY bim_item_price DESC 
																			LIMIT 0, 1
																			" ;
																			$sel_rec_res = mysql_query($sel_rec_qry);
																			$sel_rec_row = mysql_num_rows($sel_rec_res);

																			if($sel_rec_row > 0){
																					
																					 $final_data = mysql_fetch_assoc($sel_rec_res);
																					 
																					 if($deduction_amount  > 0 ){ 
																					 		 
																							echo '<br/>';	
																							echo '***bbb*** '.$deduction_amount.'***bbb***';
																							echo '<br/>';
																							
																						 	 $deduction_amount = $deduction_amount -  $final_data['bim_item_quantity_price'];		// Reduce Final loop

																							 echo $delete_query = "DELETE FROM bill_item_master  
																								WHERE bim_id = '".$final_data['bim_id']."'
																								";
																							 $delete_query_res = mysql_query($delete_query);
																							 
																							 
																						}else{
																						
																								echo '@@vvv@@ '.$deduction_amount;
																								// In between loop  deduction amount goes minus 
																								 return $deduction_amount;
																								 exit;
																								 //return abs($deduction_amount);
																						}	 
																			}
															}		
															echo '<br/>';
													}
													// end while 
													
										}else{
												
												//echo $deduction_amount;
												//echo $sel_top_bill_qry; exit;
												//exit;
												return $deduction_amount;
												exit;
												// AT LAST
										}
										
										
										// Recursive Function Call ST -----------------------
										if($deduction_amount > 0){
												make_deduction($deduction_amount,$original_amount);
										}
										// Recursive Function Call EN -----------------------
									
					
					
							}
	
	}else{
	
				// when not more then 0
				return $deduction_amount;
				exit;
	}
}

function make_deduction($deduction_amount,$original_amount){

    $condition = " billid IN ( SELECT billid FROM bill_master WHERE bill_status = 'nottaken' )  ";
	
	if( $deduction_amount  >  0){
					$sel_max_bill_qry = "SELECT * FROM bill_item_master WHERE $condition  AND  bim_item_quantity > 1 ORDER BY bim_item_quantity_price DESC ";
					$sel_max_bill_res = mysql_query($sel_max_bill_qry);
					$sel_max_bill_row = mysql_num_rows($sel_max_bill_res);
					if($sel_max_bill_row > 0)
					{
							
							while( $row = mysql_fetch_assoc($sel_max_bill_res)){
								
								
										// 1250
										if($deduction_amount  > $row['bim_item_quantity_price']    &&   $deduction_amount  > 0 ){
												
												$new_qnt = $row['bim_item_quantity'] - 1;		// Deduct total number of quantity 
												$new_bim_item_price = $row['bim_item_price'] - $row['bim_item_quantity_price'] ;		// Deduct total number of final price 
												$deduction_amount = $deduction_amount -  $row['bim_item_quantity_price'];		// Reduce Final loop
												
												$update_query = "UPDATE bill_item_master SET 
													bim_item_quantity = '".$new_qnt ."' ,
													bim_item_price = '".$new_bim_item_price."'
													WHERE bim_id = '".$row['bim_id']."'
													";
												$update_query_res = mysql_query($update_query);

										}else{
										
												//echo $sel_max_bill_qry; exit;
												
												return $deduction_amount;
												exit;
										
										}
							}
							
							
							// Recursive Function Call ST -----------------------
							if($deduction_amount > 0){
									make_deduction($deduction_amount,$original_amount);
							}
							// Recursive Function Call EN -----------------------
						
					}else{
		
										$condition = " billid IN ( SELECT billid FROM bill_master WHERE bill_status = 'nottaken' )  ";
										
										// WHEN only one quantity left 
										$sel_top_bill_qry = 
										'SELECT bim_id, billid, 
										count(billid) as itmcnt, 
										sum(bim_item_price) as billtotal 
											FROM bill_item_master 
										WHERE '. $condition.' 
										GROUP by billid
										HAVING count(billid) > 1
										ORDER by billtotal DESC';
			
										$sel_top_bill_res = mysql_query($sel_top_bill_qry);
										$sel_top_bill_row = mysql_num_rows($sel_top_bill_res);
			
										if($sel_top_bill_row > 0   &&   $deduction_amount  > 0 ){
													
													while( $top_row = mysql_fetch_assoc($sel_top_bill_res)){
															if($top_row['itmcnt']  > 1 ){
															
																			$sel_rec_qry = "SELECT * FROM bill_item_master WHERE billid = '".$top_row['billid']."' 
																			ORDER BY bim_item_price DESC 
																			LIMIT 0, 1
																			" ;
																			$sel_rec_res = mysql_query($sel_rec_qry);
																			$sel_rec_row = mysql_num_rows($sel_rec_res);

																			if($sel_rec_row > 0){
																					
																					 $final_data = mysql_fetch_assoc($sel_rec_res);
																					 
																					 if($deduction_amount  > 0 ){ 
																					 		 
																						 	 $deduction_amount = $deduction_amount -  $final_data['bim_item_quantity_price'];		// Reduce Final loop

																							 $delete_query = "DELETE FROM bill_item_master  
																								WHERE bim_id = '".$final_data['bim_id']."'
																								";
																							 $delete_query_res = mysql_query($delete_query);
																							 
																							 
																						}else{
																						
																								// In between loop  deduction amount goes minus 
																								 return $deduction_amount;
																								 exit;
																								 //return abs($deduction_amount);
																						}	 
																			}
															}		
															// echo '<br/>';
													}
													// end while 
													
										}else{
												
												//echo $deduction_amount;
												//echo $sel_top_bill_qry; exit;
												//exit;
												return $deduction_amount;
												exit;
												// AT LAST
										}
										
										
										// Recursive Function Call ST -----------------------
										if($deduction_amount > 0){
												make_deduction($deduction_amount,$original_amount);
										}
										// Recursive Function Call EN -----------------------
									
					
					
							}
	
	}else{
	
				// when not more then 0
				return $deduction_amount;
				exit;
	}
}


function create_order_id($odate,$ctime,$oid=0){

//	$dttime = date('dm', strtotime($odate));	// 2704
	$dttime = date('dm', strtotime($odate))+12;	// 2716
	
	$order_id = "";
	$order_id .= "SHIV";
	$order_id .= $dttime;
	$order_id .= $oid;
	$order_id .= str_replace(":","",$ctime);
	//$order_id = uniqid(true);
	//$order_id = mt_rand();
	
	return $order_id;
	
}

function temp_table($fieldname,$recid){
	$fieldname_val = "";

	$sel_rec_qry = "SELECT ".$fieldname." FROM temp_bill_master WHERE billid = '".$recid."' ";
	$sel_rec_res = mysql_query($sel_rec_qry);
	$sel_rec_row = mysql_num_rows($sel_rec_res);
	if($sel_rec_res > 0){
		$sel_rec_data = mysql_fetch_assoc($sel_rec_res);
		$fieldname_val = $sel_rec_data[$fieldname];
	} 
	
	return $fieldname_val;
}

function sanitize($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                   "}", "\\", "|", ";", ":", "\"", "'", ",", "<", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
        $clean;
}

function get_image_name($itemid){
	$item_image = "";

	$sel_rec_qry = "SELECT item_image FROM item_master WHERE itemid = '".$itemid."' ";
	$sel_rec_res = mysql_query($sel_rec_qry);
	$sel_rec_row = mysql_num_rows($sel_rec_res);
	if($sel_rec_res > 0){
		$sel_rec_data = mysql_fetch_assoc($sel_rec_res);
		$item_image = $sel_rec_data["item_image"];
	} 
	
	return $item_image;
	
}

function orders_records_array($remaining_amount){
	$total_sum = 0;
	$cnt = 0;
	$fixed_amt = 100; // To avoid all record searching and breaking using(checking with) this val 
	$billid_rec = array();
	$sel_billid_qry = "SELECT billid, bill_total FROM temp_bill_master ORDER BY RAND(), bill_total ASC";
	//$sel_billid_qry = "SELECT billid, bill_total FROM temp_bill_master ORDER BY bill_total ASC,RAND() ";
	$sel_billid_res = mysql_query($sel_billid_qry);
	$sel_billid_row = mysql_num_rows($sel_billid_res);
	$remaining_amount1 = $remaining_amount;
	if($sel_billid_row > 0 ){
		while($billarray = mysql_fetch_assoc($sel_billid_res)){

				$cnt++;
				// echo '<br/> '.$cnt;
				
				
				if($remaining_amount < 0){
					break;
				}
				
				//if(( ($total_sum + $billarray["bill_total"]) + 100 ) >= $remaining_amount){
				if(( ($total_sum + $billarray["bill_total"]) + 300 ) >= $remaining_amount1){	
					 continue;  
				}
			
				else{
					$billid_rec[] = $billarray["billid"];
					$total_sum += $billarray["bill_total"];
					$remaining_amount = $remaining_amount-$billarray["bill_total"];
				}
				
				
				
		}	
		
	}
	
	return $billid_rec;	
}

function max_order_date(){
	$max_date = "";
	$sel_max_date_qry = "select max(DATE_FORMAT(bill_date,'%d-%m-%Y')) as max_date_order from bill_master"; 
	
	$sel_max_date_qry = "select DATE_FORMAT(bill_date,'%d-%m-%Y') as max_date_order from bill_master
	ORDER BY bill_date DESC
	LIMIT 0,1";
	
	$sel_max_date_res = mysql_query($sel_max_date_qry);
	$sel_max_date_row = mysql_num_rows($sel_max_date_res);
	if($sel_max_date_row > 0){
		$max_date_row = mysql_fetch_assoc($sel_max_date_res);
			$max_date = $max_date_row["max_date_order"];
	}
	
	return $max_date;
}

function select_qnt()
{
	$sel = '';
	$sel .= '<select name="item_qnt" id="item_qnt" >';
	for($i=1;$i<100;$i++){
		$sel .= '<option value="'.$i.'">'.$i.'</option>';
	}	
	$sel .= '</select>';	
	
	echo $sel;
}


function select_total_orders($order_date)
{
	$order_cnt = "0";
	$sel_max_date_order_qry = "SELECT count(*) as cnt FROM `bill_master` billid WHERE bill_date like '%".$order_date."%' ";
	$sel_max_date_order_res = mysql_query($sel_max_date_order_qry);
	$sel_max_date_order_row = mysql_num_rows($sel_max_date_order_res);
	if($sel_max_date_order_row > 0){
		
		$dt_row = mysql_fetch_assoc($sel_max_date_order_res);	
		$order_cnt = $dt_row["cnt"];	
	}

	return $order_cnt;
}

function select_hometaken_orders($order_date)
{
	// nottaken   and   billtaken
	$order_cnt = "0";
	$sel_max_date_order_qry = "SELECT count(*) as cnt FROM `bill_master` billid WHERE bill_date like '%".$order_date."%' AND bill_status = 'billtaken' ";
	$sel_max_date_order_res = mysql_query($sel_max_date_order_qry);
	$sel_max_date_order_row = mysql_num_rows($sel_max_date_order_res);
	if($sel_max_date_order_row > 0){
		
		$dt_row = mysql_fetch_assoc($sel_max_date_order_res);	
		$order_cnt = $dt_row["cnt"];	
	}

	return $order_cnt;
}

function select_not_hometaken_orders($order_date)
{
	// nottaken   and   billtaken
	$order_cnt = "0";
	$sel_max_date_order_qry = "SELECT count(*) as cnt FROM `bill_master` billid WHERE bill_date like '%".$order_date."%' AND bill_status = 'nottaken' ";
	$sel_max_date_order_res = mysql_query($sel_max_date_order_qry);
	$sel_max_date_order_row = mysql_num_rows($sel_max_date_order_res);
	if($sel_max_date_order_row > 0){
		
		$dt_row = mysql_fetch_assoc($sel_max_date_order_res);	
		$order_cnt = $dt_row["cnt"];	
	}

	return $order_cnt;
}

function max_home_taken_order_id($order_date){
	$max_hometaken_order_id = "";
	$sel_max_date_qry = "select * from bill_master WHERE bill_date like '%".$order_date."%' AND bill_status = 'billtaken' ORDER BY billid DESC LIMIT 0,1";
	
	$sel_max_date_res = mysql_query($sel_max_date_qry);
	$sel_max_date_row = mysql_num_rows($sel_max_date_res);
	if($sel_max_date_row > 0){
		$max_date_row = mysql_fetch_assoc($sel_max_date_res);
			$max_hometaken_order_id = $max_date_row["billid"];
	}
	
	return $max_hometaken_order_id;
}

function select_min_required_not_home_taken($order_date,$max_home_taken_order_id){
	
	$min_required_orders = "0";
	$sel_max_date_qry = "select count(*) as cnt 
	from bill_master 
	WHERE bill_date like '%".$order_date."%' AND 
	bill_status = 'nottaken' AND
	billid <= '".$max_home_taken_order_id."' 
	";
	
	$sel_max_date_res = mysql_query($sel_max_date_qry);
	$sel_max_date_row = mysql_num_rows($sel_max_date_res);
	if($sel_max_date_row > 0){
		$max_date_row = mysql_fetch_assoc($sel_max_date_res);
			$min_required_orders = $max_date_row["cnt"];
	}
	
	return $min_required_orders;
}

function last_order_bill_master(){
	$last_order_id = "";
	
	$last_order_id_qry = "select billid from bill_master ORDER BY billid DESC LIMIT 0,1";
	$last_order_id_res = mysql_query($last_order_id_qry);
	$last_order_id_row = mysql_num_rows($last_order_id_res);
	if($last_order_id_row > 0){
		$last_order_id_rec = mysql_fetch_assoc($last_order_id_res);
			$last_order_id = $last_order_id_rec["billid"];
	}
	
	return $last_order_id;
}


function last_order_bill_item_master(){
	$last_order_id = "";
	
	$last_order_id_qry = "select bim_id from bill_item_master ORDER BY bim_id DESC LIMIT 0,1";
	$last_order_id_res = mysql_query($last_order_id_qry);
	$last_order_id_row = mysql_num_rows($last_order_id_res);
	if($last_order_id_row > 0){
		$last_order_id_rec = mysql_fetch_assoc($last_order_id_res);
			$last_order_id = $last_order_id_rec["bim_id"];
	}
	
	return $last_order_id;
}

?>