<?php
	$filename = "Manage Items";
	
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");
	
	$sel_qry = "SELECT count(*) as cnt FROM item_master";
	if ($ins_qry_res = mysql_query($sel_qry))
	{
		$fetch_rec = mysql_fetch_assoc($ins_qry_res);
		$num_rows = $fetch_rec["cnt"];
	}
		
	$itemsPerPage = 20;
	if(isset($_GET['page']) && $_GET['page'] != ""){
		$currentPage = $_GET['page'];
		
	}else{
		
		$currentPage = '0';
	}
	$totalItems = $num_rows;
	if($currentPage >0 ){
		$startAt = $itemsPerPage * ($currentPage - 1);
	}else{
		$startAt = 0;
	}	
	$cnt = $startAt;
	$urlPattern = 'list_items.php?page=(:num)';
	
	$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
	
	
	$sel_data_qry = "SELECT * FROM 	item_master  limit $startAt, $itemsPerPage";
	if ($ins_qry_data_res = mysql_query($sel_data_qry))
	{ 	// echo "New record created successfully";
		$num_rows = mysql_num_rows($ins_qry_data_res);
		
	}else{  echo "Error: " . $sel_qry . "<br>" . mysql_error($db);  }
	
?>
  
  <script type="text/javascript">
	function ConfirmDelete(itemid){

		if(!confirm('Are you sure you want to delete this record ?')) {
			return;
		}
		
			document.frm_additem.action="item_db.php";
			document.frm_additem.myaction.value = "delete";
			document.frm_additem.itemid.value = itemid ;
			document.frm_additem.submit();
		
	}
</script>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?=$filename?>
            <small>it all starts here</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?=$filename?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Item</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
            
            <!--MSG_ST-->
             	<?php
                if(isset($_REQUEST['err']) && $_REQUEST['err'] == "del"){
                    $msg = "Details has been deleted";
                }else if(isset($_REQUEST['err']) && $_REQUEST['err'] == "ins"){
                    $msg = "Details has been added";
                }else if(isset($_REQUEST['err']) && $_REQUEST['err'] == "none"){
                    $msg = "Details updated successfully";
                }else if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == "success"){
                    $msg = "Details updated successfully";
                }
                ?>
                
                <?php 
                if( $msg != ""  && ( $_REQUEST['err'] == "none"  ||  $_REQUEST['msg'] == "success"  ||  $_REQUEST['err'] == "ins" ) ){ 
                ?> 
                <script type="text/javascript">
                $( document ).ready(function() {
                    swal("Details updated successfully!", "", "success")
                 });			
                </script>
                <?php
                } ?>
            <!--MSG_EN-->
            
			 <!--Add_Item_ST-->
			<div class="col-md-6">
			 <form role="form" class="form-horizontal"  action="item_db.php" name="frm_additem" id="frm_additem" method="post" enctype="multipart/form-data" >
			 	<input type="hidden" name="myaction" id="myaction"  value="additem" />
			 	<input type="hidden" name="itemid" id="itemid"  value="" />
			 
				  <div class="box-body">
	
					<div class="form-group">
						  <label class="col-sm-3 control-label" for="inputEmail3">Item Name</label>
						  <div class="col-sm-9">
							  <input type="text" placeholder="Enter Item Name" id="itemname" name="itemname" class="form-control" required >
							</div>
					</div>
					
					 <div class="form-group">
						  <label class="col-sm-3 control-label" for="inputEmail3">Item Price</label>
						  <div class="col-sm-9">
							  <input type="text" placeholder="Enter Item Price" id="itemprice" name="itemprice" class="form-control" required>
							</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">1 Kg Price:</label>
						<div class="col-sm-9">
						<input id="kgPrice" type="number" value="0" name="kgPrice" required="">
						Rs.
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Price Type:</label>
						<div class="col-sm-9">
							<select id="price_type" name="price_type">
								<option value="setIceCreamItemId">Unit and 1 Kg</option>
								<option selected="" value="setItemId">Only Unit</option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Display Sequence:</label>
						<div class="col-sm-9">
							<input type="number" value="" name="itemSeq" id="itemSeq">
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Background Color:</label>
						<div class="col-sm-9">
							<input type="color" value="" id="backColor" class="color" name="backColor" >
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Visible:</label>
						<div class="col-sm-9">
							<select name="visible" id="visible">
								<option value="0">Hiden</option>
								<option selected="" value="1">Visible</option>
							</select> 
						</div>
					</div>
					
					
					 <div class="form-group">
						  <label class="col-sm-3 control-label" for="inputEmail3">Notes</label>
						  <div class="col-sm-9">
							  <input type="text" placeholder="Enter Item Price" id="itemnotes" name="itemnotes" class="form-control" >
							</div>
					</div>
					
                    <div class="form-group">
                      <label class="col-sm-3 for="exampleInputFile">File input</label>
                      <div class="col-sm-9">
                      	<input type="file" onchange="readURL(this);" id="item_image" name="item_image" >
                      	<p class="help-block">Please select image for product.</p>
                        <div class="display-img"><img id="blah" scr="" width="150" alt="itemimage" /></div>
					  </div>	
                    </div>
					
				  </div>
              <!-- /.box-body -->

              <div class="box-footer col-md-9" align="center">
                <button class="btn btn-primary" type="submit">Add Item</button>
              </div>
            </form>
				<div class="clear">&nbsp;&nbsp;</div>
			</div>
			 <!--Add_Item_EN-->
			
			<!--DATA_TABLE_ST-->
			<div class="row">
			
            <div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet light bordered">
			<hr style="border-width: 5px;">
            <div class="portlet-title">
			<div class="caption font-dark"> <span class="caption-subject bold uppercase">&nbsp;</span> </div>
			</div>
			
            <div class="portlet-body">
			<div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
			
			<div class="table-scrollable">
			<table border="1" id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" role="grid" aria-describedby="sample_1_info">
			<thead>
			  <tr role="row" align="center" class="text_center">
				<th rowspan="1" colspan="1" style="width: 68px;" aria-label="">
				</th>
				<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Name  </th>
				<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Price  </th>
				<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Image  </th>                
				<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Notes </th>						
				<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> 
					Edit 
				</th>																																				
				<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> 
					Delete 
				</th>	
				
			  </tr>
			</thead>
			<tbody>
			<?php
				
				
				if($num_rows > 0){
				   
					while($info = mysql_fetch_assoc($ins_qry_data_res)){
					$cnt++;							
						/*
							echo '<pre>';
							print_r($info);
							echo '</pre>';						
						*/
			?>                     
			  <tr class="gradeX odd" role="row">
				<td><?php echo $cnt; //$info['aid']; ?></td>
				<td class="text_center">
					<?php 
						echo $info['itemname']; 
						
					?>
                   
				</td>
               
				 <td align="right" >
					<?php echo $info['itemprice']; ?>
				</td>
                 <td align="center">
                	 <?php 
					 	$item_img =  $info['item_image'];
						if($item_img != ""){
					 ?>
                		 <a class="img_gallery" href="items_uploads/<?php echo $item_img;?>">
                         <img src="image_thumb.php?src=items_uploads/<?php echo $item_img;?>&q=100&h=30" >
                         </a>
                     <?php
						}
					 ?>
                </td>
				<td align="center">
					<?php 
						echo $info['itemnotes']; 
					?>							
				</td>
				<td align="center">
						<a href="manage_item.php?myaction=edit&iid=<?php echo $info['itemid']; ?>" style="text-decoration:none;">
							<span class="label label-sm label-success"> <strong>Edit </strong></span>
						</a>	
				</td>
				<td align="center">
						<a onclick="ConfirmDelete('<?php echo $info['itemid']; ?>');" href="javascript:;" style="text-decoration:none;">
							<span class="label label-sm label-success"> <strong>Delete</strong></span>
						</a>	
				</td>
			  </tr>
			<?php
				}
				}else{
					//echo 'No Records Found!';
				}
			?>
			</tbody>
			</table>
			</div>
			
			<div align="center">    
				<?php 
				  echo $paginator; 
				?>
			</div>
			
			</div>
			</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
			</div>
			</div>
			<!--DATA_TABLE_EN-->
						 
			</div><!-- /.box-body -->
			<div class="box-footer">
              
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div>
	  <!-- /.content-wrapper -->
	
 <?php
	include_once("includes/footer.php");
?>
<script>
    
    $('.display-img').hide();
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(150);
            };
            
            reader.readAsDataURL(input.files[0]);
        	$('.display-img').show();
        }
    }
</script>
      
